<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;

use app\models\Unit;
use app\models\Event;
use app\models\Ukoly;
use app\models\Season;
use app\models\FunkcePersonUnit;
use app\models\Document;

use yii\helpers\Json;
use \yii2fullcalendar\models\Event as CalEvent;

class SiteController extends BaseController
{
    public $freeAccessActions = ['index', 'about', 'orj','dokumenty','onas', 'kalendar','jsoncalendar','khms','khmsnavody','khmsstredisko'];


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ] + parent::behaviors();
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $model = $this->findUnitModel(84);

        $aktuality = Yii::$app->db->createCommand("(SELECT id,
                                                           'news' type,
                                                           valid_from date,
                                                           -1 as sign,
                                                           ABS(TO_DAYS(valid_from)-TO_DAYS(NOW())) as dist 
                                                        FROM news WHERE (TO_DAYS(valid_from)-TO_DAYS(NOW()))<=0)
                                                    UNION
                                                    (SELECT id,
                                                            'document' type,
                                                            valid_from date,
                                                            -1 sign,
                                                            ABS(TO_DAYS(created_at)-TO_DAYS(NOW())) dist
                                                        FROM document WHERE (TO_DAYS(created_at)-TO_DAYS(NOW()))<=0)
                                                    UNION
                                                    (SELECT id,
                                                            'ukoly' type,
                                                            termin date, 
                                                            1 sign,
                                                            ABS(TO_DAYS(termin)-TO_DAYS(NOW())) dist
                                                        FROM ukoly WHERE (TO_DAYS(termin)-TO_DAYS(NOW()))>=0)
                                                    UNION
                                                    (SELECT id,
                                                            'event' type,
                                                            start_date date,
                                                            1 sign,
                                                            ABS(TO_DAYS(start_date)-TO_DAYS(NOW())) dist
                                                        FROM event WHERE (TO_DAYS(start_date)-TO_DAYS(NOW()))>=0)
                                                    UNION 
                                                    /* info o vlozeni akce do DB */                                                                                                           
                                                    (SELECT id,
                                                            'event' type,
                                                            created_at date,
                                                            -1 sign,
                                                            ABS(TO_DAYS(created_at)-TO_DAYS(NOW())) dist
                                                        FROM event WHERE show_zalozeni_notification = 1
                                                        AND (TO_DAYS(created_at)-TO_DAYS(NOW()))<=0
                                                        AND (ABS(TO_DAYS(created_at)-TO_DAYS(NOW()))<ABS(TO_DAYS(prihlaseni_date)-TO_DAYS(NOW()))
                                                        	OR prihlaseni_date IS NULL)
                                                        AND (ABS(TO_DAYS(created_at)-TO_DAYS(NOW()))<ABS(TO_DAYS(start_date)-TO_DAYS(NOW()))
                                                        	OR start_date IS NULL))
                                                    UNION
                                                    /* info o terminu prihlaseni na akci */
                                                    (SELECT id,
                                                            'prihlaseni' type,
                                                            prihlaseni_date date,
                                                            1 sign,
                                                            ABS(TO_DAYS(prihlaseni_date)-TO_DAYS(NOW())) dist
                                                        FROM event WHERE (TO_DAYS(prihlaseni_date)-TO_DAYS(NOW()))>=0)
                                                    ORDER BY dist LIMIT 8")->queryAll();

        return $this->render('index',['model' => $model, 'resultChartData'=>Season::GetCurrentSeason()->overalChartData, 'aktuality'=>$aktuality]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        return $this->render('contact');
    }

    public function actionAbout()
    {
        $model = $this->findUnitModel(84);

        return $this->render('about', ['model' => $model]);
    }
    
    public function actionOnas()
    {
        $model = $this->findUnitModel(84);

        $strediska = Unit::Find()
    	//-> where(['type' => 'Středisko'])
    	//-> Where (['super_Unit_id' => 84])
    	-> Where (['like', 'Code', '715'])
    	-> orderBy('Code')
    	-> all();
        
        return $this->render('onas', ['model' => $model, 'strediska' => $strediska]);
    }
    
    public function actionDokumenty()
    {
        $model = $this->findUnitModel(84);
        
        $dokument = Document::Find()
        	//-> where(['id' => 3])
        	-> orderBy('valid_from DESC')
        	-> all();

        $zapisyDataProvider = new ActiveDataProvider([
            'query' => Document::find()->where(['type' => 'zapis'])->orderBy('id DESC')
        ]);
        
 
        return $this->render('dokumenty', ['model' => $model, 'dokument' => $dokument, 'zapisyDataProvider' => $zapisyDataProvider]);
    }
    
    public function actionOrj()
    {
	    $model = $this->findUnitModel(84);

        $clenoveRady = [];

        //Funkce Okresu
        $funkceOkres = FunkcePersonUnit::find()->where(['id_unit' => 84])->orderBy('order')->all();
	    foreach ($funkceOkres as $f) {
            if (!$f->funkce->isOrjMember) continue;
            if (array_key_exists($f->person->id, $clenoveRady))
            {
                $clenoveRady[$f->person->id]['funkce'][] = $f->funkce;
            }
            else
            {
                $clenoveRady[$f->person->id] = ['osoba' => $f->person, 'funkce' => [$f]];
            }
        }

        //Funkce v podrizenych jednotkach
        $funkceOstatni = FunkcePersonUnit::find()->where(['not', ['id_unit'=> 84]])->orderBy('order')->all();
        foreach ($funkceOstatni as $f) {
            if (!$f->funkce->isOrjMember || !$model->isParentOf($f->unit)) continue;

            if (array_key_exists($f->person->id, $clenoveRady))
            {
                $clenoveRady[$f->person->id]['funkce'][] = $f;
            }
            else
            {
                $clenoveRady[$f->person->id] = ['osoba' => $f->person, 'funkce' => [$f]];
            }
        }


        return $this->render('orj', ['model' => $model, 'clenoveRady' => $clenoveRady]);
    }

    public function actionKalendar()
    {
        return $this->render('kalendar');
    }

    
    public function actionKhms()
    {
        return $this->render('khms');
    }

    public function actionKhmsnavody()
    {
        return $this->render('khmsnavody');
    }
    
    public function actionKhmsstredisko($aktualni_stredisko = 9,$season=-1)
    {
        $rocnik = Season::findOne($season);
        if (!$rocnik)
        {
            $rocnik = Season::GetCurrentSeason();
        }

        return $this->render('khmsstredisko', ['rocnik'=> $rocnik, 'aktualni_stredisko' => $aktualni_stredisko]);
    }
    
    public function actionJsoncalendar($start=NULL,$end=NULL,$_=NULL)
    {
        $udalosti = Event::find()->where(['and',['>=','end_date', $start], ['<=', 'start_date', $end]])->all();

        $events = [];
        $idCounter = 0;
        foreach ($udalosti as $row) {
            $Event = new CalEvent();
            $Event->id = $idCounter;
            $Event->title = $row->title;
            $Event->description = $row->description;
            //$Event->allDay = true;
            $Event->start = $row->start_date;
            $Event->end = $row->end_date;

            $events[] = $Event; 
            $idCounter = $idCounter + 1;
        }

        $ukoly = Ukoly::find()->where(['and',['>=','termin', $start], ['<=', 'termin', $end]])->all();
        foreach ($ukoly as $row) {
            $Event = new CalEvent();
            $Event->id = $idCounter;
            $Event->title = $row->title;
            $Event->description = $row->description;
            $Event->allDay = true;
            $Event->start = $row->termin;
            $Event->color = 'red';

            $events[] = $Event; 
            $idCounter = $idCounter + 1;
        }


        header('Content-type: application/json');
        echo Json::encode($events);
 
        Yii::$app->end();
    }

    /**
     * Finds the Unit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Unit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUnitModel($id)
    {
        if (($model = Unit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
