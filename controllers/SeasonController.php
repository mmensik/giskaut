<?php

namespace app\controllers;

use Yii;
use app\models\Result;
use app\models\Season;
use app\models\SeasonParticipant;
use app\models\SeasonKriteria;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use yii\helpers\VarDumper;

/**
 * SeasonController implements the CRUD actions for Season model.
 */
class SeasonController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ] + parent::behaviors();
    }

    /**
     * Lists all Season models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Season::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Season model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Season model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Season();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Season model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->participant_ids = ArrayHelper::GetColumn(
                        $model->seasonParticipants,
                        'unit_id'
                    );

        $model->kriteria_ids = ArrayHelper::GetColumn(
                        $model->seasonKriterias,
                        'kriteria_id'
                    );

        if ($model->load(Yii::$app->request->post()))
        {
            $model->save();
            //Update Participants

            $old_participant_ids = $model->participant_ids;
            $new_participant_ids = Yii::$app->request->post()['Season']['participant_ids'];

            $to_create = array_diff($new_participant_ids, $old_participant_ids);

            foreach ($to_create as $newParticipantId) {
                $participant = new SeasonParticipant();
                $participant->unit_id = $newParticipantId;
                $participant->season_id = $model->id;
                $participant->save();
            }

            $to_delete = array_diff($old_participant_ids, $new_participant_ids);
            SeasonParticipant::deleteAll(['season_id' => $model->id, 'unit_id' => $to_delete]);

            $model->participant_ids = $new_participant_ids;

            //Update Kriteria

            $old_kriteria_ids = $model->kriteria_ids;
            $new_kriteria_ids = Yii::$app->request->post()['Season']['kriteria_ids'];

            $to_create = array_diff($new_kriteria_ids, $old_kriteria_ids);
            foreach ($to_create as $newKriteriaId) {
                $kriteria = new SeasonKriteria();
                $kriteria->kriteria_id = $newKriteriaId;
                $kriteria->season_id = $model->id;
                $kriteria->save();
            }

            $to_delete = array_diff($old_kriteria_ids, $new_kriteria_ids);
            SeasonKriteria::deleteAll(['season_id' => $model->id, 'kriteria_id' => $to_delete]);

            $model->kriteria_ids = $new_kriteria_ids;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionResults($id)
    {

        if (array_key_exists('Result', Yii::$app->request->post()))
        {
            $results = Yii::$app->request->post()['Result'];

            foreach ($results as $krit_id => $results_by_kriteria) {
                foreach ($results_by_kriteria as $unit_id => $result) {

                    if ($model = Result::findOne(['season_id' => $id, 'season_kriteria_id' => $krit_id, 'season_participant_id' => $unit_id]))
                    {
                        $model->points = $result['points'];
                        $model->update();
                    }
                    else
                    {
                        $model = new Result(['season_id' => $id, 'season_kriteria_id' => $krit_id, 'season_participant_id' => $unit_id, 'points'=>$result['points']]);
                        $model->save();
                    }
                }
            }

            return $this->redirect(['view', 'id' => $id]);
        }

        $model = $this->findModel($id);

        $kriterias = $model->kriterias;
        $units = $model->units;



        $results = array();
        foreach ($kriterias as $krit) {
            $row = array();
            foreach ($units as $unit) {
                $row[$unit->id] = new Result(['points' => 0]);
            }
            $results[$krit->id] = $row;
        }



        $existingResults = $model->results;

        foreach ($existingResults as $exResult) {
            $results[$exResult->season_kriteria_id][$exResult->season_participant_id] = $exResult;
        }

        return $this->render('results', [
            'model' => $model, 'results'=>$results
        ]);
    }

    /**
     * Deletes an existing Season model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Season model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Season the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Season::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
