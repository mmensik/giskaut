<?php

namespace app\controllers;


use Yii;
use app\models\Unit;
use app\models\Registration;
use app\models\Season;
use app\models\Result;
use app\models\KriteriaGroup;

use yii\web\NotFoundHttpException;

use yii\helpers\ArrayHelper;

class ChartController extends BaseController
{

    public $freeAccessActions = ['zakladna-kategorie', 'zakladna-pohlavi', 'zakladna-vek', 'zakladna-vyvoj', 'results-overall', 'unit_kriteria', 'unit_kriteria_table'];

    public function actionZakladnaKategorie($id)
    {
        $model = $this->findUnitModel($id);
        $years = Registration::GetYears();
        $cathegories = Registration::GetCathegories();

        $data = [];

        foreach ($years as $year)
        {
            $row['year'] = $year;

            foreach ($cathegories as $name => $cathegory)
            {
                $row[$name] = (int)$model->getAllMembers($year, $cathegory)->count();
            }

            $data[] = $row;
        }

        $this->setTopMenu($model);

        return $this->render('zakladna-kategorie', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionZakladnaPohlavi($id)
    {
        $model = $this->findUnitModel($id);
        $this->setTopMenu($model);

        return $this->render('zakladna-pohlavi', [
            'model' => $model,
            'data' => $model->zakladnaByPohlavi,
        ]);
    }

    public function actionZakladnaVek($id)
    {
        $year = Registration::CURRENT_YEAR;
        $model = $this->findUnitModel($id);

        $result = $model->getAllMembers($year)
                        ->select(["$year - YEAR(birthday) as 'age'"])
                        ->orderBy('age')
                        ->asArray()->all();
                    

        $data = [];
        for ($i = 0; $i < 100; $i++) $data[$i] = 0;
        foreach ($result as $row)
        {
            $data[$row['age']]++;
        }

        $filteredData[] = [];
        foreach ($data as $key => $row)
        {
            if ($row > 0) $filteredData[] = ['age'=>(int)$key, 'headCount'=>(int)$row];
        }

        $this->setTopMenu($model);
        
        return $this->render('zakladna-vek', [
            'model' => $model,
            'data' => $filteredData,
        ]);
    }

    public function actionZakladnaVyvoj($id)
    {
        $model = $this->findUnitModel($id);
        $years = Registration::GetYears();
        $units = $model->units;

        $headCounts = [];
        foreach ($years as $year)
        {
            $hc['year'] = $year; 
            $hc[$model->id] = (int)$model->getAllMembers($year)->count();

            foreach ($units as $unit)
            {
                $hc[$unit->id] = (int)$unit->getAllMembers($year)->count();
            }

            $headCounts[] = $hc;
        }

        $this->setTopMenu($model);

        return $this->render('zakladna-vyvoj', [
            'model' => $model,
            'data' => $headCounts,
        ]);
    }

    public function actionResultsOverall($seasonId = -1)
    {
        $rawData;
        if ($seasonId == -1) // sezona -1 znamena, ze nebyla zadna nastavena
        {
            $season = Season::GetCurrentSeason();
        }
        else
        {
            if ($season = Season::findOne($seasonId)) {

            } else throw new NotFoundHttpException('Dany rocnik neexistuje'); 
        }
       
        $rawData = $season->resultsPerGroup; 

       // var_dump($rawData);

        $dataPerUnit = [];

        // Preskladani do struktury vhodne pro graf
        foreach ($rawData as $row) {
            if (!array_key_exists($row['unit_id'], $dataPerUnit))
            {
                $dataPerUnit[$row['unit_id']] = [];
            }
            $dataPerUnit[$row['unit_id']][strval($row['kategorie'])] = doubleval($row['body']);
        }

        $data = [];
        foreach ($dataPerUnit as $unit_id => $row) {
            $row['unit'] = Unit::findOne($unit_id)->name;
            $row['unit_short'] = Unit::findOne($unit_id)->short_name;
            $data[] = $row;
        }


        $kriterias = $season->kriterias;
        $units = $season->units;

        $results = array();
        foreach ($kriterias as $krit) {
            $row = array();
            foreach ($units as $unit) {
                $row[$unit->id] = new Result(['points' => 0]);
            }
            $results[$krit->id] = $row;
        }

        $existingResults = $season->results;

        foreach ($existingResults as $exResult) {
            $results[$exResult->season_kriteria_id][$exResult->season_participant_id] = $exResult;
        }
        
        $seasons = Season::find()->all();


        return $this->render('resultsOverall', [
            'data' => $data, 'results' => $results, 'kriterias' => $kriterias, 'units'=>$units, 'season'=>$season, 'seasons' => $seasons
        ]);
    }
    
    public function actionResultsOverall2()
    {
        return $this->render('resultsOverall2', [
            'data' => Season::GetCurrentSeason()->overalChartData,
        ]);
    }
    
    public function actionUnit_kriteria($id, $kategorie_id)
    {
        $unit_id = $id;
        $unit = $this->findUnitModel($unit_id);
        $kategorie = KriteriaGroup::findOne($kategorie_id);
        
        $data = array_filter(Season::GetCurrentSeason()->GetResultsPerGroup('body'),
                             function($row) use ($kategorie_id) {
                                return $row['kategorie'] == $kategorie_id;
                             });

        $line_value = 0;

        foreach ($data as $row_id => $row) {
            if ($row['unit_id'] == $unit_id)
            {
                $data[$row_id]['color'] = '#FFD02D';
                $line_value = $row['body'];
            }
            else
            {
                $data[$row_id]['color'] = '#9E9E9E';
            }
                        
        }

        $this->setTopMenuByKategories($unit);

        return $this->render('unit_kriteria', ['model' => $unit, 'dataKriterium' => array_values($data), 'kategorie' => $kategorie, 'line_value' => $line_value]);
    }
    
    
    public function actionUnit_kriteria_table($id)
    {
        $unit_id = $id;
        $unit = $this->findUnitModel($unit_id);
        $kategorie = KriteriaGroup::find()->all();
        
        $data = array_filter(Season::GetCurrentSeason()->GetResultsPerGroup('body'),
                             function($row) use ($unit_id) {
                                return $row['unit_id'] == $unit_id;
                             });

        /*
        $line_value = 0;

        foreach ($data as $row_id => $row) {
            if ($row['unit_id'] == $unit_id)
            {
                $data[$row_id]['color'] = '#FFD02D';
                $line_value = $row['body'];
            }
            else
            {
                $data[$row_id]['color'] = '#9E9E9E';
            }
            
            if ($row['kategorie'] == $kategorie_id)
            {
	            $data_graph[]=$row;	        	 	   
            }
                        
        }
        */

        $this->setTopMenuByKategories($unit);

        return $this->render('unit_kriteria_table', ['model' => $unit, 'dataStredisko' => array_values($data), 'kategorie' => array_values($kategorie)]);
    }
    
    
    

    protected function setTopMenu($model)
    {
        $this->view->params['topMenu'] =
        [
            [
                'label' => 'Vývoj',
                'url' => ['chart/zakladna-vyvoj', 'id' => $model->id],
            ],
            [
                'label' => 'Podle pohlaví',
                'url' => ['chart/zakladna-pohlavi', 'id' => $model->id],
            ],
            [
                'label' => 'Podle kategorie',
                'url' => ['chart/zakladna-kategorie', 'id' => $model->id],
            ],
            [
                'label' => 'Věkové spektrum',
                'url' => ['chart/zakladna-vek', 'id' => $model->id],
            ],
        ];
    }

    protected function setTopMenuByKategories($model)
    {
        $kriterias = KriteriaGroup::find()->all();

        $menu = [];

        foreach ($kriterias as $key => $value) {
            $menu[] = [
                'label' => $value->name,
                'url' => ['chart/unit_kriteria', 'id'=> $model->id, 'kategorie_id' => $value->id]
            ];
        }

        $this->view->params['topMenu'] = $menu;
    }

    /**
     * Finds the Unit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Unit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUnitModel($id)
    {
        if (($model = Unit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
