<?php

namespace app\controllers;

use Yii;

use app\models\UploadForm;
use app\models\Person;
use app\models\Unit;
use app\models\Registration;

use yii\web\Controller;
use yii\web\UploadedFile;

class UploadController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionUpload()
    {
        $model = new UploadForm();

        $status = [];

        if ($model->load(Yii::$app->request->post())) {
        	
        	$model->year = Yii::$app->request->post()['UploadForm']['year'];
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->file && $model->validate()) {

            	$name = 0;
            	$surname = 1;
            	$nickname = 2;
            	$birthday = 3;
            	$rc = 4;
            	$sex = 5;
            	$street = 6;
            	$city = 7;
            	$zip = 8;
            	$country = 9;
            	$functions = 10;
            	$qualifications = 11;
            	$oddil_code = 12;
            	$oddil_name = 13;
            	$oddil_type =14;
            	$reg_type = 15;
            	$cathegory = 16;

            	$file = fopen($model->file->tempName, 'r');

            	while (!feof($file))
            	{
            		$line = fgetcsv($file, 0, ';');

            		if (!$line[0]) continue;
            		
            		$person = Person::find()->where(['rc'=>$line[$rc]])->one();

            		if (!$person)
            		{
            			$person = new Person();
            		}
            		$person->rc = $line[$rc];
            		$person->name = $line[$name];
            		$person->surname = $line[$surname];
            		$person->nickname = $line[$nickname];
            		$person->street = $line[$street];
            		$person->city = $line[$city];
            		$person->country = $line[$country];
            		$person->zip = $line[$zip];
            		$person->sex = $line[$sex] == "Muž" ? 'male' : 'female';
            		$date_str = str_replace('.','-',str_replace(' ', '', $line[$birthday]));
            		$date_php = date_create_from_format("j-n-Y", $date_str);
            		$person->birthday = Yii::$app->formatter->asDate($date_php, 'php:Y-m-d');
            		$person->cathegory = $line[$cathegory];
            		if (!$person->save())
                        {
                            foreach ($person->getErrors() as $key => $value)
                            $status[] = [
                                'entity' => 'Person',
                                'name' => $person->name . " " . $person->surname,
                                'message' => implode($value),
                            ];
                        }




            		$codeArr = explode('.', $line[$oddil_code]);
            		$unit_id = 0;

                    $stredisko_code= $codeArr[0] .'.'.$codeArr[1];
                    $stredisko = Unit::find()->where(['code'=>$stredisko_code])->one();
                    if (!$stredisko)
                    {
                        $stredisko = new Unit();
                        $stredisko->code = $stredisko_code;
                        $stredisko->type = "Středisko";
                        if (!$stredisko->save())
                        {
                            foreach ($stredisko->getErrors() as $key => $value)
                            $status[] = [
                                'entity' => 'Stredisko',
                                'name' => $person->name . " " . $person->surname,
                                'message' => implode($value),
                            ];
                        }
                    }

                    if ($line[$oddil_type] == "Středisko") {
                        $unit_id = $stredisko->id;
                        if (!$stredisko->name or $stredisko->name = "")
                        {
                            $stredisko->name = $line[$oddil_name];
                            $stredisko->save();
                        }
                    } else {
                        $o_code= $codeArr[0] .'.'.$codeArr[1] . '.' .$codeArr[2];
	            		$oddil = Unit::find()->where(['code'=>$o_code])->one();
	            		if (!$oddil)
	            		{
	            			$oddil = new Unit();
	            			$oddil->super_unit_id = $stredisko->id;
	            			$oddil->code = $o_code;
	            			$oddil->name = $line[$oddil_name];
	            			$oddil->type = "Oddíl";
	            			if (!$oddil->save())
                            {
                                foreach ($oddil->getErrors() as $key => $value)
                                $status[] = [
                                    'entity' => 'Oddil',
                                    'name' => $person->name . " " . $person->surname,
                                    'message' => implode($value),
                                ];
                            }
	            		}

                        if ($line[$oddil_type] == "Oddíl") {
	            		    $unit_id = $oddil->id;
                            if (!$oddil->name or $oddil->name = "")
                            {
                                $oddil->name = $line[$oddil_name];
                                $oddil->save();
                            }
                        } else {
                            $druzina = Unit::find()->where(['code'=>$line[$oddil_code]])->one();
                            if (!$druzina)
                            {
                                $druzina = new Unit();
                                $druzina->super_unit_id = $oddil->id;
                                $druzina->code = $line[$oddil_code];
                                $druzina->name = $line[$oddil_name];
                                $druzina->type = "Družina";
                                if (!$druzina->save())
                                {
                                    foreach ($druzina->getErrors() as $key => $value)
                                    $status[] = [
                                        'entity' => 'Druzina',
                                        'name' => $person->name . " " . $person->surname,
                                        'message' => implode($value),
                                    ];
                                }
                            }

                            $unit_id = $druzina->id;
                            
                            if (!$druzina->name or $druzina->name = "")
                            {
                                $druzina->name = $line[$oddil_name];
                                $druzina->save();
                            }
                        }
            		}

            		$year = $model->year;

            		$registration = Registration::find()->where(['person_id'=>$person->id, 'unit_id'=>$unit_id, 'year'=>$year])->one();
            		if (!$registration)
            		{
            			$registration = new Registration();
            			$registration->year = $year;
            			$registration->unit_id = $unit_id;
            			$registration->person_id = $person->id;
            			$registration->cathegory = $line[$cathegory];
            			if (!$registration->save())
                        {
                            foreach ($registration->getErrors() as $key => $value)
                            $status[] = [
                                'entity' => 'Registration',
                                'name' => $person->name . " " . $person->surname,
                                'message' => implode($value),
                            ];
                        }
            		}
            	}
            	fclose($file); 
            }
        }

        return $this->render('upload', ['model' => $model, 'status'=>$status]);
    }

}
