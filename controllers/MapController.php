<?php

namespace app\controllers;

use app\models\Unit;
use app\models\Registration;
use app\models\Klubovna;

class MapController extends BaseController
{

    public $freeAccessActions = ['heat', 'klubovny'];

    public function actionHeat($id)
    {
    	$years = Registration::GetYears();
        $model = $this->findUnitModel($id);

        $this->setTopMenu($model);

        return $this->render('heat', [
            'model' => $model,
        	'years' => $years,
        ]);
    }

    public function actionKlubovny($id)
    {
    	$years = Registration::GetYears();
        $model = $this->findUnitModel($id);

        $klubovny = Klubovna::find()->all();

        $this->setTopMenu($model);

        return $this->render('klubovny', [
            'model' => $model,
        	'years' => $years,
        	'klubovny' => $klubovny,
        ]);
    }

    protected function findUnitModel($id)
    {
        if (($model = Unit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function setTopMenu($model)
    {
        $this->view->params['topMenu'] =
        [
            [
                'label' => 'Heatmap',
                'url' => ['map/heat', 'id' => $model->id],
            ],
            [
                'label' => 'Klubovny',
                'url' => ['map/klubovny', 'id' => $model->id],
            ],
        ];
    }

}
