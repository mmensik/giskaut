<?php

namespace app\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\web\View;

/**
 * Google Charts widget class
 */
class GoogleMap extends Widget
{
	public $id;

	public $width;

	public $height;

	public function init()
    {
        parent::init();

        ob_start();
    }

    public function run()
    {
        $script = ob_get_clean();
        $script = str_replace(["<script>","</script>"], "", $script);
        
        $view = $this->getView();
        $view->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyC-0ySrdapWeOB2JwywCH_ZXjMwmHEFBxs&libraries=visualization&sensor=true_or_false', ['position' => View::POS_HEAD]);
		$view->registerJs($script, View::POS_HEAD);

		return "<div id='$this->id' style='width: $this->width; height: $this->height;'></div>";
    }
}
?>