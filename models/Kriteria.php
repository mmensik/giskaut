<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kriteria".
 *
 * @property integer $id
 * @property string $short_description
 * @property string $description
 * @property integer $difficulty
 *
 * @property SeasonKriteria[] $seasonKriterias
 * @property Season[] $idSeasons
 */
class Kriteria extends GenKriteria
{
   public static function ListKriterias()
    {
        $data = Kriteria::find()->select(['id', 'short_description'])->all();
        return ArrayHelper::map($data, 'id', 'short_description');
    }
}
