<?php

namespace app\models;


use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is helper class to filter queries based on a year (used in "via" for registrations)
 */
class RegistrationFilter
{
    public $year = null;

    public $units = null;

    public $cathegories = null;

    public function filterQuery($query)
    {
        $where = [];
        if ($this->year) $where['year'] = $this->year;
        if ($this->units) $where['unit_id'] = $this->units;
        if ($this->cathegories) $where['cathegory'] = $this->cathegories;
        return $query->where($where);
            
    }
}

/**
 * This is the model class for table "unit".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $super_unit_id
 * @property string $type
 *
 * @property Unit $superUnit
 * @property Unit[] $units
 */
class Unit extends GenUnit
{
    public function getAllSubunitsIds()
    {
        $ids = [$this->id];

        $units = $this->units;
        foreach ($units as $unit)
        {
            $subIds = $unit->getAllSubunitsIds();
            $ids = array_merge($subIds, $ids);
        }
        return $ids;
    }

    public function getMembers($year)
    {
        $regFilter = new RegistrationFilter();
        $regFilter->year = $year;

        return $this->hasMany(Person::className(),
            ['id' => 'person_id'])->via('registrations', [$regFilter, 'filterQuery']);
    }

    public function getAllMembers($year, $cathegories = null)
    {
        $regFilter = new RegistrationFilter();
        $regFilter->year = $year;
        $regFilter->units = $this->allSubunitsIds;
        $regFilter->cathegories = $cathegories;

        return $this->hasMany(Person::className(),
            ['id' => 'person_id'])->via('allRegistrations', [$regFilter, 'filterQuery']);
    }

    public function getAllRegistrations()
    {
        $query =  Registration::find();
        $query->multiple = true;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKlubovna()
    {
        if ($this->klubovna_id)
        {
            return $this->hasOne(Klubovna::className(), ['id' => 'klubovna_id']);   
        } elseif ($this->super_unit_id)
        {
            return $this->superUnit->klubovna;
        }
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuperUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'super_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Unit::className(), ['super_unit_id' => 'id']);
    }

    public function getZakladnaByPohlavi()
    {
        $years = Registration::GetYears();
        $data = [];

        foreach ($years as $year)
        {
            $dataRow['year'] = $year;

            $result = ArrayHelper::map($this->getAllMembers($year)->select('count(*) as hc, sex')->groupBy('sex')->asArray()->all(),
                                    'sex','hc');
            
            $dataRow['male'] = array_key_exists('male', $result) ? $result['male'] : 0;
            $dataRow['female'] = array_key_exists('female', $result) ? $result['female'] : 0;

            $data[] = $dataRow;
        }

        return $data;
    }

    public function isParentOf($unit)
    {
        if ($this == $unit) return true;
        if (!$unit->super_unit_id) return false;

        return $this->isParentOf($unit->superUnit);
    }

    public static function GetSuperCathegory()
    {
        return [
            'Družina'=>'Oddíl',
            'Oddíl'=>'Středisko',
            'Středisko'=>'Okres',
            'Okres'=>''
        ];
    }

    public static function ContainsCathegory($units, $cathegory)
    {
        foreach ($units as $unit) {
            if ($unit->type == $cathegory) return true;
        }
        return false;
    }

    public static function ListStredisko()
    {
        $data = Unit::find()->select(['id', 'name'])->where(['type'=>'Středisko'])->all();
        return ArrayHelper::map($data, 'id', 'name');
    }

    public static function GetTypesArray()
    {
        return [ 'Oddíl' => 'Oddíl', 'Středisko' => 'Středisko', 'Družina' => 'Družina', 'Okres' => 'Okres', ];
    }

    public static function GetORJSumperk()
    {
        return Unit::findOne(84);
    }
}
