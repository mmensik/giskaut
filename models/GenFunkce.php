<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "funkce".
 *
 * @property integer $id
 * @property string $name
 * @property string $isOrjMember
 *
 * @property FunkcePersonUnit[] $funkcePersonUnits
 */
class GenFunkce extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'funkce';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'isOrjMember'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'isOrjMember' => 'Is Orj Member',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFunkcePersonUnits()
    {
        return $this->hasMany(FunkcePersonUnit::className(), ['id_funkce' => 'id']);
    }
}
