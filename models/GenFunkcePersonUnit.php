<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "funkce_person_unit".
 *
 * @property integer $id_funkce
 * @property integer $id_person
 * @property integer $id_unit
 *
 * @property Funkce $idFunkce
 * @property Unit $idUnit
 * @property Person $idPerson
 */
class GenFunkcePersonUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'funkce_person_unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_funkce', 'id_person', 'id_unit'], 'required'],
            [['id_funkce', 'id_person', 'id_unit'], 'integer'],
            [['id_funkce'], 'exist', 'skipOnError' => true, 'targetClass' => Funkce::className(), 'targetAttribute' => ['id_funkce' => 'id']],
            [['id_unit'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::className(), 'targetAttribute' => ['id_unit' => 'id']],
            [['id_person'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['id_person' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_funkce' => 'Id Funkce',
            'id_person' => 'Id Person',
            'id_unit' => 'Id Unit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFunkce()
    {
        return $this->hasOne(Funkce::className(), ['id' => 'id_funkce']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'id_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'id_person']);
    }
}
