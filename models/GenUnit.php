<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $short_name
 * @property integer $super_unit_id
 * @property string $type
 * @property integer $klubovna_id
 * @property string $web
 * @property string $email
 *
 * @property FunkcePersonUnit[] $funkcePersonUnits
 * @property Registration[] $registrations
 * @property SeasonParticipant[] $seasonParticipants
 * @property Season[] $seasons
 * @property Klubovna $klubovna
 * @property GenUnit $superUnit
 * @property GenUnit[] $genUnits
 */
class GenUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'type'], 'required'],
            [['super_unit_id', 'klubovna_id'], 'integer'],
            [['type'], 'string'],
            [['code'], 'string', 'max' => 16],
            [['name', 'email'], 'string', 'max' => 64],
            [['short_name'], 'string', 'max' => 45],
            [['web'], 'string', 'max' => 128],
            [['code'], 'unique'],
            [['klubovna_id'], 'exist', 'skipOnError' => true, 'targetClass' => Klubovna::className(), 'targetAttribute' => ['klubovna_id' => 'id']],
            [['super_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => GenUnit::className(), 'targetAttribute' => ['super_unit_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'short_name' => 'Short Name',
            'super_unit_id' => 'Super Unit ID',
            'type' => 'Type',
            'klubovna_id' => 'Klubovna ID',
            'web' => 'Web',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFunkcePersonUnits()
    {
        return $this->hasMany(FunkcePersonUnit::className(), ['id_unit' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrations()
    {
        return $this->hasMany(Registration::className(), ['unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeasonParticipants()
    {
        return $this->hasMany(SeasonParticipant::className(), ['unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeasons()
    {
        return $this->hasMany(Season::className(), ['id' => 'season_id'])->viaTable('season_participant', ['unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKlubovna()
    {
        return $this->hasOne(Klubovna::className(), ['id' => 'klubovna_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuperUnit()
    {
        return $this->hasOne(GenUnit::className(), ['id' => 'super_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenUnits()
    {
        return $this->hasMany(GenUnit::className(), ['super_unit_id' => 'id']);
    }
}
