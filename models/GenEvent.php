<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property string $prihlaseni_date
 * @property integer $show_zalozeni_notification
 * @property string $created_at
 * @property string $updated_at
 */
class GenEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['start_date', 'end_date', 'prihlaseni_date', 'created_at', 'updated_at'], 'safe'],
            [['show_zalozeni_notification'], 'integer'],
            [['title'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'prihlaseni_date' => 'Prihlaseni Date',
            'show_zalozeni_notification' => 'Show Zalozeni Notification',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
