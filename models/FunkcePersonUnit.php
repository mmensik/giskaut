<?php

namespace app\models;

use Yii;

class FunkcePersonUnit extends GenFunkcePersonUnit
{
	    public function attributeLabels()
    {
        return [
            'id_funkce' => 'Funkce',
            'id_person' => 'Osoba',
            'id_unit' => 'Id Unit',
        ];
    }

        /**
     * @return \yii\db\ActiveQuery
     */
    public function getFunkce()
    {
        return $this->hasOne(Funkce::className(), ['id' => 'id_funkce']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'id_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'id_person']);
    }

}