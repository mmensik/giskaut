<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person".
 *
 * @property integer $id
 * @property string $rc
 * @property string $name
 * @property string $surname
 * @property string $nickname
 * @property string $birthday
 * @property string $sex
 * @property string $telephone
 * @property string $city
 * @property string $street
 * @property string $country
 * @property integer $zip
 * @property string $cathegory
 * @property double $lat
 * @property double $lng
 *
 * @property Klubovna[] $klubovnas
 * @property Registration[] $registrations
 */
class GenPerson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rc', 'name', 'surname', 'birthday', 'sex'], 'required'],
            [['birthday'], 'safe'],
            [['zip'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['rc'], 'string', 'max' => 12],
            [['name', 'surname', 'nickname', 'city'], 'string', 'max' => 32],
            [['sex'], 'string', 'max' => 11],
            [['telephone'], 'string', 'max' => 16],
            [['street'], 'string', 'max' => 45],
            [['country'], 'string', 'max' => 20],
            [['cathegory'], 'string', 'max' => 64],
            [['rc'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rc' => 'Rc',
            'name' => 'Name',
            'surname' => 'Surname',
            'nickname' => 'Nickname',
            'birthday' => 'Birthday',
            'sex' => 'Sex',
            'telephone' => 'Telephone',
            'city' => 'City',
            'street' => 'Street',
            'country' => 'Country',
            'zip' => 'Zip',
            'cathegory' => 'Cathegory',
            'lat' => 'Lat',
            'lng' => 'Lng',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKlubovnas()
    {
        return $this->hasMany(Klubovna::className(), ['spravce_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrations()
    {
        return $this->hasMany(Registration::className(), ['person_id' => 'id']);
    }
}
