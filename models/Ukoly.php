<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ukoly".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $termin
 */
class Ukoly extends GenUkoly
{
	public static $DAYS_BEFORE_ALERT = 4;

	public function getSentMailsCount()
	{
		return $this->hasMany(MailSent::className(), ['reason_id' => 'id'])->andOnCondition(['reason' => 'ukol_alert'])->count();
	}

	public static function GetUkolyForAlert()
	{
		return Ukoly::find()->where('DATEDIFF(termin, NOW()) < ' . Ukoly::$DAYS_BEFORE_ALERT . ' and termin > NOW()')->all();
	}
}
