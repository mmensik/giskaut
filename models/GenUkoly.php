<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ukoly".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $termin
 */
class GenUkoly extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ukoly';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['termin'], 'safe'],
            [['title'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'termin' => 'Termin',
        ];
    }
}
