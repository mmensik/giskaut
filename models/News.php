<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $obsah
 * @property string $date_created
 * @property string $link
 */
class News extends GenNews
{
}
