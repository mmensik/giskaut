<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $file
 * @property integer $user_created
 * @property string $type
 * @property string $valid_from
 * @property string $created_at
 * @property string $updated_at
 */
class GenDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['user_created'], 'integer'],
            [['type'], 'string'],
            [['valid_from', 'created_at', 'updated_at'], 'safe'],
            [['title', 'description', 'file'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'file' => 'File',
            'user_created' => 'User Created',
            'type' => 'Type',
            'valid_from' => 'Valid From',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
