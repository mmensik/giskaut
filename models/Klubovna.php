<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "klubovna".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $city
 * @property string $street
 * @property string $country
 * @property integer $zip
 * @property double $lat
 * @property double $lng
 *
 * @property Unit[] $units
 */
class Klubovna extends GenKlubovna
{
    public function GetAllUnits()
    {
    	$result = [];
    	foreach ($this->units as $unit) {
    		if ($unit->getAllMembers(Registration::CURRENT_YEAR)->count() > 0) $result[] = $unit;
    		$this->getSubUnits($unit, $result);
    	}

    	return $result;
    }

    private function getSubUnits($unit, &$result)
    {
    	foreach ($unit->units as $subUnit)
    	{
    		if ($subUnit->klubovna_id == null) {
    			if ($subUnit->getAllMembers(Registration::CURRENT_YEAR)->count() > 0) $result[] = $subUnit;
    			$this->getSubUnits($subUnit, $result);
    		}
    	}
    }
}
