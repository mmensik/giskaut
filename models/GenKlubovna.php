<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "klubovna".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $spravce_id
 * @property string $city
 * @property string $street
 * @property string $country
 * @property integer $zip
 * @property double $lat
 * @property double $lng
 *
 * @property Person $spravce
 * @property Unit[] $units
 */
class GenKlubovna extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'klubovna';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['spravce_id', 'zip'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['name', 'street'], 'string', 'max' => 45],
            [['city'], 'string', 'max' => 32],
            [['country'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'spravce_id' => 'Spravce ID',
            'city' => 'City',
            'street' => 'Street',
            'country' => 'Country',
            'zip' => 'Zip',
            'lat' => 'Lat',
            'lng' => 'Lng',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpravce()
    {
        return $this->hasOne(Person::className(), ['id' => 'spravce_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Unit::className(), ['klubovna_id' => 'id']);
    }
}
