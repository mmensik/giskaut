<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "result".
 *
 * @property integer $season_id
 * @property integer $season_kriteria_id
 * @property integer $season_participant_id
 * @property double $points
 *
 * @property SeasonKriteria $season
 * @property SeasonParticipant $season0
 */
class GenResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['season_id', 'season_kriteria_id', 'season_participant_id'], 'required'],
            [['season_id', 'season_kriteria_id', 'season_participant_id'], 'integer'],
            [['points'], 'number'],
            [['season_id', 'season_kriteria_id'], 'exist', 'skipOnError' => true, 'targetClass' => SeasonKriteria::className(), 'targetAttribute' => ['season_id' => 'season_id', 'season_kriteria_id' => 'kriteria_id']],
            [['season_id', 'season_participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => SeasonParticipant::className(), 'targetAttribute' => ['season_id' => 'season_id', 'season_participant_id' => 'unit_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'season_id' => 'Season ID',
            'season_kriteria_id' => 'Season Kriteria ID',
            'season_participant_id' => 'Season Participant ID',
            'points' => 'Points',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(SeasonKriteria::className(), ['season_id' => 'season_id', 'kriteria_id' => 'season_kriteria_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason0()
    {
        return $this->hasOne(SeasonParticipant::className(), ['season_id' => 'season_id', 'unit_id' => 'season_participant_id']);
    }
}
