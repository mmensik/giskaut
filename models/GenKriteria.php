<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kriteria".
 *
 * @property integer $id
 * @property string $short_description
 * @property string $description
 * @property integer $difficulty
 * @property integer $kriteria_group_id
 * @property double $points_per_unit
 * @property string $unit
 * @property double $basic_unit
 * @property string $termin
 *
 * @property KriteriaGroup $kriteriaGroup
 * @property SeasonKriteria[] $seasonKriterias
 * @property Season[] $seasons
 */
class GenKriteria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kriteria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['short_description', 'difficulty'], 'required'],
            [['short_description', 'description'], 'string'],
            [['difficulty', 'kriteria_group_id'], 'integer'],
            [['points_per_unit', 'basic_unit'], 'number'],
            [['termin'], 'safe'],
            [['unit'], 'string', 'max' => 128],
            [['kriteria_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => KriteriaGroup::className(), 'targetAttribute' => ['kriteria_group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'difficulty' => 'Difficulty',
            'kriteria_group_id' => 'Kriteria Group ID',
            'points_per_unit' => 'Points Per Unit',
            'unit' => 'Unit',
            'basic_unit' => 'Basic Unit',
            'termin' => 'Termin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKriteriaGroup()
    {
        return $this->hasOne(KriteriaGroup::className(), ['id' => 'kriteria_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeasonKriterias()
    {
        return $this->hasMany(SeasonKriteria::className(), ['kriteria_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeasons()
    {
        return $this->hasMany(Season::className(), ['id' => 'season_id'])->viaTable('season_kriteria', ['kriteria_id' => 'id']);
    }
}
