<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "oddil".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $stredisko_id
 *
 * @property Registration[] $registrations
 */
class Oddil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oddil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'stredisko_id'], 'required'],
            [['stredisko_id'], 'integer'],
            [['code'], 'string', 'max' => 16],
            [['name'], 'string', 'max' => 32],
            [['code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'stredisko_id' => 'Stredisko ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrations()
    {
        return $this->hasMany(Registration::className(), ['oddil_id' => 'id']);
    }
}
