<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $obsah
 * @property string $valid_from
 * @property string $link
 */
class GenNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['obsah'], 'string'],
            [['valid_from'], 'safe'],
            [['title'], 'string', 'max' => 45],
            [['link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'obsah' => 'Obsah',
            'valid_from' => 'Valid From',
            'link' => 'Link',
        ];
    }
}
