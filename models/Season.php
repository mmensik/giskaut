<?php

namespace app\models;

use Yii;

use yii\db\Query;

/**
 * This is the model class for table "season".
 *
 * @property integer $id
 * @property string $name
 * @property integer $isActive
 *
 * @property SeasonKriteria[] $seasonKriterias
 * @property Kriteria[] $idKriterias
 * @property SeasonParticipant[] $seasonParticipants
 * @property Unit[] $idUnits
 */
class Season extends GenSeason
{
	public $participant_ids = [];

	public $kriteria_ids = [];


	public function GetResults()
	{
		return Result::find()->where(['season_id' => $this->id])->with('kriteria')->all();
	}

	public static function GetCurrentSeason()
	{
		return Season::find()->where(['isActive' => 1])->orderBy(['id' => SORT_DESC])->limit(1)->one();
	}

	public function GetResultsPerGroup($orderBy = 'unit.code')
	{

		$query = new Query;

		return $query->select(['season_participant_id as unit_id', 'unit.short_name as short_name', 'kriteria.kriteria_group_id as kategorie', 'ROUND(SUM(points * kriteria.points_per_unit),2) as body'])
			->from('result')
			->where(['season_id'=>$this->id])
			->leftJoin('kriteria','kriteria.id = season_kriteria_id')
			->leftJoin('unit', 'unit.id = season_participant_id')
			->groupBy('kriteria.kriteria_group_id, season_participant_id')
			->orderBy($orderBy)
			->all();
	}

	public function GetOveralChartData()
	{
		$rawData = $this->resultsPerGroup;

        $dataPerUnit = [];

        foreach ($rawData as $row) {
            if (!array_key_exists($row['unit_id'], $dataPerUnit))
            {
                $dataPerUnit[$row['unit_id']] = [];
            }
            $dataPerUnit[$row['unit_id']][strval($row['kategorie'])] = doubleval($row['body']);
        }

        $data = [];
        foreach ($dataPerUnit as $unit_id => $row) {
        	$unit =  Unit::findOne($unit_id);
            $row['unit_name'] = $unit->name;
            $row['unit_code'] = $unit->code;
            $row['unit_short'] = $unit->short_name;
            $data[] = $row;
        }

        return $data;
	}
}
