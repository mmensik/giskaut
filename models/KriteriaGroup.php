<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kriteria_group".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Kriteria[] $kriterias
 */
class KriteriaGroup extends GenKriteriaGroup
{

    public static function GetDropdownItems()
    {
        $groups = KriteriaGroup::find()->all();

        $list = [];

        foreach ($groups as $group) {
            $list[$group->id] = $group->name;
        }

        return $list;
    }
  
}
