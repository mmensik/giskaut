<?php

namespace app\models;

use Yii;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 */
class Event extends GenEvent
{
    public function behaviors()
	{
	    return [
	    	[
	        'class' =>TimestampBehavior::className(),
	        'value'=> new Expression('NOW()')
	        ]
	    ];
	}

	    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),
        [
            'show_zalozeni_notification' => 'Notifikace o zalozeni',
        ]);
    }
}
