<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mail_sent".
 *
 * @property integer $id
 * @property string $reason
 * @property integer $reason_id
 * @property string $subject
 * @property string $adresses
 * @property string $body
 * @property string $created_at
 */
class GenMailSent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mail_sent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reason'], 'required'],
            [['reason', 'adresses', 'body'], 'string'],
            [['reason_id'], 'integer'],
            [['created_at'], 'safe'],
            [['subject'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reason' => 'Reason',
            'reason_id' => 'Reason ID',
            'subject' => 'Subject',
            'adresses' => 'Adresses',
            'body' => 'Body',
            'created_at' => 'Created At',
        ];
    }
}
