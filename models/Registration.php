<?php

namespace app\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "registration".
 *
 * @property integer $person_id
 * @property integer $unit_id
 * @property integer $year
 * @property string $cathegory
 *
 * @property Person $person
 * @property Unit $unit
 */
class Registration extends \yii\db\ActiveRecord
{

     const CURRENT_YEAR = 2016;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'registration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'unit_id', 'year', 'cathegory'], 'required'],
            [['person_id', 'unit_id', 'year'], 'integer'],
            [['cathegory'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_id' => 'Person ID',
            'unit_id' => 'Unit ID',
            'year' => 'Year',
            'cathegory' => 'Cathegory',
        ];
    }

    public static function GetYears()
    {
        return ArrayHelper::getColumn(Registration::find()->groupBy("year")->orderBy("year")->all(),'year');
    }

    public static function GetCathegories()
    {
        return [
                    'Benjamínek' => ['Benjamínek'],
                    'Vlčata a světlušky' => ['Vlče','Světluška'],
                    'Skaut/Skautka' => ['Skaut','Skautka'],
                    'RR' => ['Ranger', 'Rover'],
                    'Ostatní' => ['Člen kmene dospělých','Ostatní','safa'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::className(), ['id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }
}
