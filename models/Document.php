<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $date_created
 * @property string $file
 * @property integer $user_created
 *
 *
 */
class Document extends GenDocument
{
	public $uploadedFile;

	public function rules()
	{
		return parent::rules() + [ 
			[['uploadedFile'], 'file']
		];
	}

	public function behaviors()
	{
	    return [
	    	[
	        'class' =>TimestampBehavior::className(),
	        'value'=> new Expression('NOW()')
	        ]
	    ];
	}

	public static function GetTypes()
	{
		return [
			'' => '',
			'zapis' => 'Zapis',
			'vyhlaska' => 'Vyhlaska',
			'vz' => 'Vyrocni zprava',
			'ostatni' => 'Jine'
		];
	}
}
