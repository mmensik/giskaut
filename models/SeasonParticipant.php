<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "season_participant".
 *
 * @property integer $id_season
 * @property integer $id_unit
 *
 * @property Unit $idUnit
 * @property Season $idSeason
 */
class SeasonParticipant extends GenSeasonParticipant
{
   
}
