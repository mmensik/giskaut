<?php

namespace app\models;

use Yii;

class Funkce extends GenFunkce
{
	public static function GetDropdownItems()
	{
		$items = Funkce::find()->all();

		$list = [];

		foreach ($items as $item) {
			$list[$item->id] =  $item->name;
		}

		return $list;
	}

	/**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        $labels['isOrjMember'] = 'Je clenem Orj';

        return $labels;
    }
}