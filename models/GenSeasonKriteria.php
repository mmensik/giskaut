<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "season_kriteria".
 *
 * @property integer $kriteria_id
 * @property integer $season_id
 *
 * @property Result[] $results
 * @property Season $season
 * @property Kriteria $kriteria
 */
class GenSeasonKriteria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'season_kriteria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kriteria_id', 'season_id'], 'required'],
            [['kriteria_id', 'season_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kriteria_id' => 'Kriteria ID',
            'season_id' => 'Season ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(Result::className(), ['season_id' => 'season_id', 'season_kriteria_id' => 'kriteria_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(Season::className(), ['id' => 'season_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKriteria()
    {
        return $this->hasOne(Kriteria::className(), ['id' => 'kriteria_id']);
    }
}
