<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "season_kriteria".
 *
 * @property integer $id_kriteria
 * @property integer $id_season
 *
 * @property Season $idSeason
 * @property Kriteria $idKriteria
 */
class SeasonKriteria extends GenSeasonKriteria 
{
 
}
