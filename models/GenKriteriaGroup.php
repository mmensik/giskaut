<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kriteria_group".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Kriteria[] $kriterias
 */
class GenKriteriaGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kriteria_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 90],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKriterias()
    {
        return $this->hasMany(Kriteria::className(), ['kriteria_group_id' => 'id']);
    }
}
