<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "season".
 *
 * @property integer $id
 * @property string $name
 * @property integer $isActive
 *
 * @property SeasonKriteria[] $seasonKriterias
 * @property Kriteria[] $kriterias
 * @property SeasonParticipant[] $seasonParticipants
 * @property Unit[] $units
 */
class GenSeason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'season';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['isActive'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'isActive' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeasonKriterias()
    {
        return $this->hasMany(SeasonKriteria::className(), ['season_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKriterias()
    {
        return $this->hasMany(Kriteria::className(), ['id' => 'kriteria_id'])->viaTable('season_kriteria', ['season_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeasonParticipants()
    {
        return $this->hasMany(SeasonParticipant::className(), ['season_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Unit::className(), ['id' => 'unit_id'])->viaTable('season_participant', ['season_id' => 'id']);
    }
}
