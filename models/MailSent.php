<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

use Yii;

/**
 * This is the model class for table "mail_sent".
 *
 * @property integer $id
 * @property string $reason
 * @property integer $reason_id
 * @property string $subject
 * @property string $adresses
 * @property string $body
 * @property string $created_at
 */
class MailSent extends GenMailSent
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    //ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],

                ],
                'value'=> new Expression('NOW()'),
            ],
        ];
    }

}
