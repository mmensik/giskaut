<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "result".
 *
 * @property integer $season_id
 * @property integer $season_kriteria_id
 * @property integer $season_participant_id
 * @property integer $points
 *
 * @property SeasonKriteria $season
 */
class Result extends GenResult 
{
	 /**
     * @return \yii\db\ActiveQuery
     */
    public function getKriteria()
    {
        return $this->hasOne(Kriteria::className(), ['id' => 'season_kriteria_id']);
    }

    public function getPointResult()
    {
    	return $this->points * $this->kriteria->points_per_unit;
    }
}
