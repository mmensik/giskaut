<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person".
 *
 * @property integer $id
 * @property string $rc
 * @property string $name
 * @property string $surname
 * @property string $nickname
 * @property string $birthday
 * @property string $sex
 * @property string $city
 * @property string $street
 * @property string $country
 * @property integer $zip
 * @property string $cathegory
 *
 * @property Registration[] $registrations
 */
class Person extends GenPerson
{

	public static function GetDropdownItems()
	{
		$persons = Person::find()->all();

		$list = [];

		foreach ($persons as $person) {
			$list[$person->id] = $person->surname . " " . $person->name;
		}

		return $list;
	}

    public function getLastUnit()
    {
        return $this->hasOne(Unit::className(),
            ['id' => 'unit_id'])->via('registrations' ,
            function($query) {
                return $query->orderBy("year DESC");
            });
    }

    public function GetFullName()
    {
    	return $this->name . ' ' . $this->surname;
    }
}
