<?php


namespace app\commands;

use Yii;
use yii\console\Controller;
use  yii\helpers\Console;

use app\models\Ukoly;
use app\models\MailSent;

class MailController extends Controller
{

    public function actionIndex()
    {
        $ukoly = Ukoly::GetUkolyForAlert();

        foreach ($ukoly as $ukol) {

            $this->stdout("Ukol - " . $ukol->title . " status : ");

            if ($ukol->sentMailsCount > 0)
            {
                $this->stdout("Alert already sent \n", Console::BOLD);
                continue;
            }

            $this->stdout("Sending alert \n", Console::FG_GREEN);

            $mail = new MailSent;

            $mail->reason = "ukol_alert";
            $mail->reason_id = $ukol->id;

            $mail->subject = "Pripominka: " . $ukol->title;

            $mail->save();
        }
    }
}