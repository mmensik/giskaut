<?php


namespace app\commands;

use Yii;
use yii\console\Controller;

class GsController extends Controller
{
	/*
    // The command "yii example/create test" will call "actionCreate('test')"
    public function actionCreate($name) { ... }

    // The command "yii example/index city" will call "actionIndex('city', 'name')"
    // The command "yii example/index city id" will call "actionIndex('city', 'id')"
    public function actionIndex($category, $order = 'name') { ... }

    // The command "yii example/add test" will call "actionAdd(['test'])"
    // The command "yii example/add test1,test2" will call "actionAdd(['test1', 'test2'])"
    public function actionAdd(array $name) { ... }
*/
    public function actionIndex($message = 'hello world')
    {
         
        $auth = Yii::$app->authManager;

        $auth->removeAllRules();

        $rule = new \app\rbac\TestRule;

        $auth->add($rule);
    }
}