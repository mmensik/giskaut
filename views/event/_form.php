<?php

use yii\helpers\Html;

use kartik\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'start_date')->widget(DateTimePicker::classname(), [
        'pluginOptions' => [
            'autoclose' => true,
           // 'format' => 'yyyy-mm-dd'
        ]
    ]); ?>

    <?= $form->field($model, 'end_date')->widget(DateTimePicker::classname(), [
        'pluginOptions' => [
            'autoclose' => true,
           // 'format' => 'yyyy-mm-dd'
        ]
    ]); ?>

    <?= $form->field($model, 'prihlaseni_date')->widget(DateTimePicker::classname(), [
        'pluginOptions' => [
            'autoclose' => true,
           // 'format' => 'yyyy-mm-dd'
        ]
    ]); ?>

    <?= $form->field($model, 'show_zalozeni_notification')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
