﻿ <?php
/* @var $this yii\web\View */
$this->title = 'GISkaut';

use app\models\Unit;
use app\models\Registration;
use app\models\Person;
use app\models\Klubovna;
use app\widgets\GoogleCharts;
use app\widgets\GoogleMap;

use yii\helpers\Html;
?>
<div class="col-md-8">

	<div class="mapa_main_page">
   <?php 


GoogleMap::begin([
                        'id' => 'map-canvas',
                        'width' => '100%',
                        'height' => '650px'
                        ]) ?>
<script>
      function initialize() {
        var activeInfo;
        var heatmapData = [
        <?php 

        $members = Unit::GetORJSumperk()->getAllMembers(Registration::CURRENT_YEAR)->all();
        $latSum = 0;
        $count = 0;
        $lngSum = 0; 
        foreach ($members as $member)
        {
          if (!$member->lat or !$member->lng or $member->lat < 40 or $member->lng < 10) continue;
          $count++;
          $latSum += $member->lat;
          $lngSum += $member->lng;
        ?>
            new google.maps.LatLng(<?= $member->lat ?> ,<?= $member->lng ?>),
            <?
        }
        ?>
        ];
        var mapCenter = new google.maps.LatLng( <?= $latSum / ($count ? $count : 1) ?>, <?= $lngSum / ($count ? $count : 1) ?>);
        var mapOptions = {
          //center: { lat: <?= $latSum / ($count ? $count : 1) ?>, lng: <?= $lngSum / ($count ? $count : 1) ?>},
          center: { lat: 50.021765, lng: 17.008613},
          zoom: 10,
          //maxZoom: 13,
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);
        var heatmap = new google.maps.visualization.HeatmapLayer({
          data: heatmapData,
          //disipating: true.
          radius: 30,
        });
        heatmap.setMap(map);

        google.maps.event.addListener(map, 'zoom_changed', function() {
          var zoomLevel = map.getZoom();
          if (zoomLevel > 13)
          {
            heatmap.setMap(null);
          } else {
            heatmap.setMap(map);
          }
        });


        <?php
        $klubovny = Klubovna::find()->all();

        foreach($klubovny as $klubovna)
        {
            if (!$klubovna->lat or !$klubovna->lng or $klubovna->lat < 40 or $klubovna->lng < 10) continue;
            $units = $klubovna->GetAllUnits();
        ?>
            var marker<?= $klubovna->id ?> = new google.maps.Marker({
                position: new google.maps.LatLng(<?= $klubovna->lat ?> ,<?= $klubovna->lng ?>),
                map: map,
                title: '<?= $klubovna->name ?>',
                icon: '<?= Unit::ContainsCathegory($units, "Středisko") ? "images/stredisko.png" : "images/oddil.png" ?>',
            });

            var contentString = '<strong><?= $klubovna->name ?></strong>'
                              + '<ul>'
            <?php
            foreach ($units as $unit) {
              if ($unit->type == 'Družina') continue;
            ?>
                              + '<li><?= $unit->type ?> <?= Html::a($unit->name, ["chart/zakladna-vyvoj", "id"=>$unit->id]) ?></li>' 
            <?php
            }
            ?>
                              + '</ul>'

            var infoWindow<?= $klubovna->id ?> = new google.maps.InfoWindow({
               content: contentString,
            });

            google.maps.event.addListener(marker<?= $klubovna->id ?>, 'click', function() {
                if (activeInfo) activeInfo.close();
               infoWindow<?= $klubovna->id ?>.open(map,marker<?= $klubovna->id ?>);
               activeInfo = infoWindow<?= $klubovna->id ?>;
            });
        <?
        }
        
        ?>

      }
      google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php GoogleMap::end() ?>
</div>&nbsp;
</div>
<div  class="col-md-4">
<h2>Střediska</h2>
<!--
	<script>
		function zobraz(idecko){
			el=document.getElementById(idecko).style; 
			el.display='block';
		}
	</script>
	<script>
		function skryj(idecko){
			el=document.getElementById(idecko).style; 
			el.display='none';
		}
	</script>
//-->
      <?php
     
      foreach ($strediska as $s) {
	   	
	      echo ($s->type=='Středisko')?'<div class="info_stredisko"><strong>'.$s->code.' '.$s->name.'</strong><br><div id="poznamka'.$s->id.'">'.((Empty($s->web))?'':'Web: <a target="_blank" href="http://'.$s->web.'">'.$s->web.'</a><br>').'Email: '.$s->email.'<br><!--Vedoucí: <br>//--></div></div>':'';
      }

?>
<div class="info_stredisko">
<? echo Html::a("kompletní seznam kontaktů","https://docs.google.com/spreadsheets/d/1NXHjlN4QbnFXatLEIb3EEn20RQSGdlbXSuyBQDifLgs/edit#gid=0",['target' => '_blank']); ?>
</div>
</div>