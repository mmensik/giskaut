﻿<?php
/* @var $this yii\web\View */
$this->title = 'GISkaut';

use app\models\Season;

?>
        <script src="amcharts/amcharts.js" type="text/javascript"></script>
        <script src="amcharts/serial.js" type="text/javascript"></script>
<h1>Kritéria hodnocení a motivace středisek</h1>

<div style="border: solid 0px red; clear: both">
      
            <?
            
            //$aktualni_stredisko=(IsSet($aktualni_stredisko))?$aktualni_stredisko:9;
            $aktualni_color="AC5938";
            $aktualni_color_main="FFD546"; //C53300
            
            //vyber aktualniho rocniku
            $aktualni_rok = $rocnik->name;
            //$aktualni_rok=(IsSet($year))?$year:$aktualni_rok;
            
            //zjisteni poctu zasedani ORJ
            $pocet_zasedani_ORJ=0;
            $pocet_zasedani_ORJs=Yii::$app->db->createCommand("select sum(points) AS suma FROM result WHERE season_id=(SELECT ID FROM season WHERE name='".$aktualni_rok."') AND season_kriteria_id=1 GROUP BY season_participant_id ORDER BY suma DESC LIMIT 1")->queryAll();;
            foreach ($pocet_zasedani_ORJs AS $pocet_zasedani_ORJx){
	            $pocet_zasedani_ORJ=$pocet_zasedani_ORJx['suma'];
            }
            
            
            //zjisteni, zda jsou pro aktualni rocnik uz data z registrace o poctech clenu
            $tmp_rok_registraces=Yii::$app->db->createCommand("SELECT count(person_id) as pocet FROM registration WHERE year=".$aktualni_rok." GROUP BY year")->queryAll();
            foreach ($tmp_rok_registraces AS $tmp_rok_registrace) {
	           $rok_registrace=($tmp_rok_registrace['pocet']>0)?$aktualni_rok:$aktualni_rok-1;
            }
            
            /*
            //zjisteni_poctu_administrativnich_ukolu
            $pocet_ukolu_help=mysql_fetch_row(mysql_query("SELECT count(*) AS pocet FROM `KHMS_body` WHERE kriterium in (SELECT ID FROM KHMS_kriteria WHERE KHMS_skupina=6) AND rocnik=".$aktualni_rok." group BY jednotka ORDER BY pocet DESC LIMIT 1"));
            $pocet_ukolu=max(1,$pocet_ukolu_help[0]);
              
            */         
			
            //$rok_registrace=2015;
            $pocet_ukolu=2;
            $pocet_stredisek=0;
            
            //nacteni dat o strediscich (nazev, cislo, kratky nazev, pocet clenu)            
            $strediska = Yii::$app->db->createCommand("SELECT ID, name, code, short_name, (SELECT count(person_id) FROM registration WHERE year=".$rok_registrace." AND (unit_id in (SELECT ID FROM unit u2 WHERE u2.super_unit_id=u1.ID OR u2.super_unit_id IN (SELECT ID FROM unit u3 WHERE u3.super_unit_id=u1.ID)) OR unit_id=u1.ID)) AS pocet FROM unit u1 WHERE type='Středisko' AND super_unit_id=84")->queryAll();
            $pocet_stredisek=0;
            foreach ($strediska as $stredisko) {
	            $strediska_data['nazev'][$stredisko['ID']]=$stredisko['name'];
            	$strediska_data['cislo'][$stredisko['ID']]=$stredisko['code'];
            	$strediska_data['zkratka'][$stredisko['ID']]=$stredisko['short_name'];
            	$strediska_data['pocet'][$stredisko['ID']]=$stredisko['pocet'];
            	
            	$pocet_stredisek++;
				//echo $stredisko['name']." ".$stredisko['code']." ".$stredisko['short_name']." ".$stredisko['pocet']."<br>";
			}

                                    
            //priprava dat pro grafy a tabulky
            $where_clause=Array(1=>
            12, 13, 16, 17,
            18, 14, "Select ID FROM kriteria WHERE kriteria_group_id = 2", 1, "19,20", 15);
            
            //nacteni bodu z DB
            for ($i=1;$i<=10;$i++):
            	$results[$i] = Yii::$app->db->createCommand("SELECT season_participant_id, sum(points) AS stav, sum(points*(SELECT points_per_unit FROM kriteria k1 WHERE k1.ID=b.season_kriteria_id)) AS body FROM result b WHERE season_id=(SELECT ID FROM season WHERE name='".$aktualni_rok."') AND season_kriteria_id IN (".$where_clause[$i].") GROUP BY season_participant_id")->queryAll();
            endfor;
            
            
            //zjisteni, zda pro dana kriteria jsou v DB uz nejake body a podle toho se rozhodne, zda se pro ne ma zobrazit graf a poradi strediska
            for ($i=1;$i<=10;$i++):
            	$tmp_results = Yii::$app->db->createCommand("SELECT sum(points) AS stav FROM result WHERE season_id=(SELECT ID FROM season WHERE name='".$aktualni_rok."') AND season_kriteria_id IN (".$where_clause[$i].")")->queryAll();
            	foreach ($tmp_results AS $tmp_result) {
	            	if ($tmp_result['stav']==0):
	            		$KHMS_data[$i]['obsahuje_data']=0;
	            	else:
	            		$KHMS_data[$i]['obsahuje_data']=1;
	            	endif;	
            	}
            endfor;

                        
            $nazev_grafu=Array(1=>
            "Procent členů mladších 18 let",
            "Táborových děťodnů na člena",
            "Počet ČZ na 10 registrovaných členů",
            "Počet VZ na 10 registrovaných členů",
            
            "Účast na závodech",
            "Registrace bez výjimek",
            "Plnění administrativních úkolů",
            "Účast na ORJ",
            "Hodnocení kvality",
            "Organizace závodů",
            
			"Body dle počtu členů (na 10 členů)",
			"Body na středisko");
            
            $nazev_grafu2=Array(1=>
            "členů mladších 18 let",
            "táborových děťodnů",
            "čekatelských zkoušek",
            "vůdcovských zkoušek",
            
            "účast na závodech",
            "registrace bez výjimek",
            "administrativních úkolů",
            "účast na ORJ",
            "hodnocení kvality",
            "organizace závodů",
            
            "Body dle počtu členů (na 10 členů)",
            "Body na středisko");
            
            $typ_grafu=Array(1=>
            "na_clena",
            "na_clena",
            "na_clena",
            "na_clena",
            
            "na_stredisko",
            "na_stredisko",
            "na_stredisko",
            "na_stredisko",
            "na_stredisko",
            "na_stredisko");
            
            $procenta[1]="%";

            $procenta[2]="";
            $procenta[3]="";
            $procenta[4]="";
            $procenta[5]="";
			$procenta[6]="";	
            
            $procenta[7]="%";
            $procenta[8]="%";
            $procenta[9]="%";

            $procenta[10]="";
            $procenta[11]="";
            $procenta[12]="";
            
            $koef=Array(1=>
            100, 1, 10, 10,
            1, 1, 100/$pocet_ukolu, 100/$pocet_zasedani_ORJ, 50, 1);
            
            //info o tom, zda se ma dana skupina kriterii objevit v seznamu oblasti s nejvetsim potencialem (tj. nejvetsi ztratou bodu na prvniho v okrese)
            $ukaz_doporuceni=Array(1=>
            "ne", "ano", "ano", "ano",
            "ano", "ano", "ano", "ano", "ano", "ne", "-", "-");
     
          	//naplneni pole KHMS_data daty z DB
            for($i=1;$i<=count($results);$i++):
            	 
            	//Kazdy SQL dotaz vrati tuto strukturu: id_strediska, stav, body
	            //K tomu budou udaje koef, typ_grafu
            	foreach ($results[$i] as $result) {
		            //echo $result['season_participant_id']." ".$result['stav']." ".$result['body']."<br>";
		            $KHMS_data[$i]['stav'][$result['season_participant_id']]=$result['stav'];
	            	$KHMS_data[$i]['body'][$result['season_participant_id']]=$result['body'];
	            	$KHMS_data[$i]['pro_graf'][$result['season_participant_id']]=($typ_grafu[$i]=="na_clena")?$koef[$i]*$result['stav']/$strediska_data['pocet'][$result['season_participant_id']]:$koef[$i]*$result['stav'];
	            }
            
	        endfor;
	        
	        $tmp_na_clena = [];
	        $tmp_na_stredisko = [];
	        $strediska_data['celkem_bodu'] = [];

	        //doplneni souhrnnych dat na clena a na stredisko, secteni celkoveho poctu bodu strediska + naplneni poli ztrata_na_prvniho_stav, ztrata_na_prvniho_body, body_pro_tabulku
	        foreach ($KHMS_data as $key => $value) {
		        foreach ($KHMS_data[$key]['body'] as $key2 => $value2) {
			        
			        if (!array_key_exists($key2, $tmp_na_clena)) $tmp_na_clena[$key2] = 0;
			        if (!array_key_exists($key2, $tmp_na_stredisko)) $tmp_na_stredisko[$key2] = 0;

			        $tmp_na_clena[$key2]+=($typ_grafu[$key]=="na_clena")?$value2:0;
			        $tmp_na_stredisko[$key2]+=($typ_grafu[$key]=="na_stredisko")?$value2:0;
			        
			        if (!array_key_exists($key2, $strediska_data['celkem_bodu'])) $strediska_data['celkem_bodu'][$key2] = 0;
			        $strediska_data['celkem_bodu'][$key2]+=$value2;
			        
			        //naplneni poli ztrata_na_prvniho_stav, ztrata_na_prvniho_body, body_pro_tabulku
			        
			        $key2_max=array_search(max($KHMS_data[$key]['pro_graf']),$KHMS_data[$key]['pro_graf']);  //vrati index (id strediska), ktere je prvni
			        
	        
			        $KHMS_data[$key]['ztrata_na_prvniho_stav'][$key2]=($typ_grafu[$key]=="na_clena")?ceil($strediska_data['pocet'][$key2]*max($KHMS_data[$key]['pro_graf'])/$koef[$key]-$KHMS_data[$key]['stav'][$key2]):round(max($KHMS_data[$key]['stav'])-$KHMS_data[$key]['stav'][$key2],2);   
			        
			        $KHMS_data[$key]['ztrata_na_prvniho_body'][$key2]= ($KHMS_data[$key]['stav'][$key2_max]==0) ? 0 : round((($typ_grafu[$key]=="na_clena")?$KHMS_data[$key]['ztrata_na_prvniho_stav'][$key2]*$KHMS_data[$key]['body'][$key2_max]/$KHMS_data[$key]['stav'][$key2_max]:$KHMS_data[$key]['body'][$key2_max]-$KHMS_data[$key]['body'][$key2]),2);
			        $KHMS_data[$key]['body_pro_tabulku'][$key2]=($typ_grafu[$key]=="na_clena")?10*$value2/$strediska_data['pocet'][$key2]:$value2;
	

		        }
	        }
	        
	        foreach ($tmp_na_clena as $key => $value) {
		        $KHMS_data[$i]['pro_graf'][$key]=10*$value/$strediska_data['pocet'][$key];
		        $KHMS_data[$i]['body'][$key]=$value;
		        $KHMS_data[$i]['body_pro_tabulku'][$key]=10*$value/$strediska_data['pocet'][$key];
	        }
	        $KHMS_data[$i]['obsahuje_data']=1;

	        
	        
	        foreach ($tmp_na_stredisko as $key => $value) {
		        $KHMS_data[$i+1]['pro_graf'][$key]=$value;
		        $KHMS_data[$i+1]['body'][$key]=$value;
		        $KHMS_data[$i+1]['body_pro_tabulku'][$key]=$value;
	        }
     		$KHMS_data[$i+1]['obsahuje_data']=1;

 
	        ?>
	        
            <script>
            var chart;
            var chartData = new Array('<? echo count($KHMS_data); ?>');
            
            <?           

            for($i=1;$i<=count($KHMS_data);$i++):
            	echo "chartData[".$i."] = [";
				
	            $j=$pocet_stredisek; // TODO ?????
	            $hodnota_polozky=-1;
	            unset($rank_tmp);
				
	            ASort ($KHMS_data[$i]['pro_graf']);
	            foreach ($KHMS_data[$i]['pro_graf'] as $key => $value) { 
	            	if ($KHMS_data[$i]['obsahuje_data']>0): //nezobrazeni kriterii, kterym v dane sezone dosud nebyly zapsany adne body
			            echo "{ \"stredisko\": \"".$strediska_data['zkratka'][$key]."\",\n\"stav\": ".round($value,($procenta[$i]=="%")?0:2).",\n\"url\": \"?r=site/khmsstredisko&year=".$aktualni_rok."&aktualni_stredisko=".$key."\"";
		            	if ($key==$aktualni_stredisko):
		            		echo (IsSet($KHMS_data[$i]['stav'][$key]))?",\n\"color\": \"#".$aktualni_color."\"":",\n\"color\": \"#".$aktualni_color_main."\"";
		               	endif;
		            	echo ",\n\"forceShow\": true";
		            	echo "\n},\n";
		            	
		           	
			            
			            //zjištění pořadí - část 1
		            	$poradi_polozky=($hodnota_polozky==$value)?$poradi_polozky:$j;
		            	$rank_tmp[$key]=$value;
		            	$index_tmp[$j]=$key;
		            	$rank_to[$i][$key]=$poradi_polozky;
	
		            	$j--;
		            	$hodnota_polozky=$value;
		            endif;
	            }
	           
	            echo "];";
				
	            //zjištění pořadí - část 2
	            $hodnota_polozky=-1;
	            for($k=1;$k<=count($rank_tmp);$k++):
	            
					$poradi_polozky=($hodnota_polozky==$rank_tmp[$index_tmp[$k]])?$poradi_polozky:$k;            	            
	            	$rank_from[$i][$index_tmp[$k]]=$poradi_polozky;
	            	$hodnota_polozky=$rank_tmp[$index_tmp[$k]];
	            	$rank[$i][$index_tmp[$k]]=($rank_from[$i][$index_tmp[$k]]==$rank_to[$i][$index_tmp[$k]])?$rank_from[$i][$index_tmp[$k]]:$rank_from[$i][$index_tmp[$k]]."-".$rank_to[$i][$index_tmp[$k]];
	            	$rank_average[$i][$index_tmp[$k]]=($rank_from[$i][$index_tmp[$k]]+$rank_to[$i][$index_tmp[$k]])/2;
	            	
	            	if ($rank_average[$i][$index_tmp[$k]]<$pocet_stredisek/5):
	            		$rank_style[$i][$index_tmp[$k]]=" style=\"color: #00B050\"";
	            		$rank_text[$i][$index_tmp[$k]]="Vynikající";
	            	elseif ($rank_average[$i][$index_tmp[$k]]<5):
	            		$rank_style[$i][$index_tmp[$k]]=" style=\"color: #CCDA46\"";
	            		$rank_text[$i][$index_tmp[$k]]="Dobré";	
	            	elseif ($rank_average[$i][$index_tmp[$k]]<7.5):
	            		$rank_style[$i][$index_tmp[$k]]=" style=\"color: #FFC000\"";
	            		$rank_text[$i][$index_tmp[$k]]="Průměr";
	            	elseif ($rank_average[$i][$index_tmp[$k]]<9.5):
	            		$rank_style[$i][$index_tmp[$k]]=" style=\"color: #E46D0A\"";
	            		$rank_text[$i][$index_tmp[$k]]="Prostor ke zlepšení";	 
	            	else:
	            		$rank_style[$i][$index_tmp[$k]]=" style=\"color: #FF0000\"";
	            		$rank_text[$i][$index_tmp[$k]]="Velký prostor ke zlepšení";	            	           			            		            		
	            	endif;	
	            	
	            endfor;
              
            endfor;	
 
           	?>
           	
            var fillAlphas=0.65;
            var lineColor="#9E9E9E";
            
            <? for($i=1;$i<=count($KHMS_data);$i++): ?>
            
            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData[<? echo $i;?>];
                chart.categoryField = "stredisko";
                chart.columnWidth = 0.6;             
                chart.startDuration = 0;

                // TITLE
                //chart.addTitle("<? echo (IsSet($KHMS_data[$i]['stav'][$key]))?"":$nazev_grafu[$i];?>", 15);
                chart.titles = [{"text":"<? echo (IsSet($KHMS_data[$i]['stav'][$key]))?"":$nazev_grafu[$i];?>", "size":"15", "color":"#5E5E5E"}];
                
                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.labelRotation = 60;
                categoryAxis.gridAlpha = 0;
                categoryAxis.gridPosition = "0";
                categoryAxis.forceShowField = "forceShow";
                //categoryAxis.tickPosition = "start";
                //categoryAxis.tickLength = 60;
                //categoryAxis.fontSize = 8;
                
                // value
               	var valueAxis = new AmCharts.ValueAxis();
                //valueAxis.title = "<? echo $nazev_grafu[$i];?>";
                valueAxis.gridAlpha = 0;
                valueAxis.axisAlpha = 0.5;
                chart.addValueAxis(valueAxis);
                // in case you don't want to change default settings of value axis,
                // you don't need to create it, as one value axis is created automatically.

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.valueField = "stav";
                graph.balloonText = "[[category]]: <b>[[value]]<? echo $procenta[$i]; ?></b>";
                graph.type = "column";
                graph.urlField = "url";
                graph.lineAlpha = 0;
                graph.fillAlphas = fillAlphas;
                graph.fillColorsField ="color";
                graph.lineColor = "<? echo (IsSet($KHMS_data[$i]['stav'][$key]))?"#D8BEAA":"#9E9E9E"; ?>"
                graph.labelText = "[[value]]<? echo $procenta[$i]; ?>"; 
                chart.addGraph(graph);

                // CURSOR
                var chartCursor = new AmCharts.ChartCursor();
                chartCursor.cursorAlpha = 0;
                chartCursor.zoomable = false;
                chartCursor.categoryBalloonEnabled = false;
                
                chart.addChartCursor(chartCursor);

                chart.creditsPosition = "top-left";

                chart.write("chartdiv<? echo $i;?>");
            });
            
            <? endfor; ?>

       
           
        </script>

    	<script>
		<!--
			function navigatorx(XPullDown) {
			location.href=(document.XPullDown.XList.options[document.XPullDown.XList.selectedIndex].value);
			}
			
			function navigatory(XPullDown) {
			location.href=(document.XPullDown.YList.options[document.XPullDown.YList.selectedIndex].value);
			}
			
		// -->
		</script>
		
		<script>
			function zobraz(idecko){
				el=document.getElementById('graf'+idecko).style; 
				el.display='block';
				el.height='100%';
				document.getElementById("odkaz"+idecko).href='javascript: skryj('+idecko+')';
				document.getElementById("sipka"+idecko).src='images/detail-mi2.gif';
				document.getElementById("radek"+idecko).onClick='skryj('+idecko+')';
			}
		</script>
		
		<script>
			var seznam_grafu = [
		 	<?
		 	KSort($nazev_grafu);
		 	foreach ($nazev_grafu as $key => $value) {
			 	echo "'".$key."'";
			 	if ($key==count($nazev_grafu)-2) { break; }
			 	echo ",";
		 	}
		 	?>
		 	
			] 
			function zobraz_pouze(idecko){
				for (i = 0; i < seznam_grafu.length; i++) { 
					elem=document.getElementById('graf'+seznam_grafu[i]).style; 
					elem.height='0';
					document.getElementById("odkaz"+seznam_grafu[i]).href='javascript: zobraz('+seznam_grafu[i]+')';
					document.getElementById("sipka"+seznam_grafu[i]).src='images/detail-mi.gif';
				    //skryj(seznam_grafu[i]);
				}
				el=document.getElementById('graf'+idecko).style; 
				el.display='block';
				el.height='100%';
				document.getElementById("odkaz"+idecko).href='javascript: skryj('+idecko+')';
				document.getElementById("sipka"+idecko).src='images/detail-mi2.gif';
				document.getElementById("radek"+idecko).onClick='skryj('+idecko+')';
				
				
				
			}
		</script>
		
		<script>
			function skryj(idecko){
				el=document.getElementById('graf'+idecko).style; 
				el.display='none';
				document.getElementById("odkaz"+idecko).href='javascript: zobraz('+idecko+')';
				document.getElementById("sipka"+idecko).src='images/detail-mi.gif';
				
			}
		</script>
		
		
		<div style="width: 1100px; overflow: hidden; border: solid 0px black; position: relative">
		<div style="width: 620px; text-align: left; border: solid 0px black; position: absolute; top: 20px; left: 482px">
			<form name="XPullDown">
			Zvol jiné středisko: 
			<select name="XList" onChange="navigatorx(this.form)" size="1">
			
			<?
			foreach ($strediska_data['cislo'] as $key => $value) {
			
				echo "<option value=\"?r=site/khmsstredisko&season=".$rocnik->id."&aktualni_stredisko=".$key."\"".(($aktualni_stredisko==$key)?" SELECTED":"").">".$strediska_data['cislo'][$key]." - ".$strediska_data['nazev'][$key]."</option>";
					
			}

			?>
			</select>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Zvol jiný rok: 
			<select name="YList" onChange="navigatory(this.form)" size="1">
			
			<?
			$rocniky = Season::find()->all();
			foreach ($rocniky as $rocnik) {
				echo "<option value=\"?r=site/khmsstredisko&season=".$rocnik->id."&aktualni_stredisko=".$aktualni_stredisko."\"".(($rocnik->name==$aktualni_rok)?" SELECTED":"").">".$rocnik->name."</option>";
			}
			
			?>
			
			</select>
			</form>
		
		</div>
		
		<h2>
		<?
		echo "Středisko ".$strediska_data['zkratka'][$aktualni_stredisko].", rok ".$aktualni_rok;
		?>
		</h2>

		<p class="normal">
		<table style="margin-left: 15px">
		<tr>
		<td class="right" width=130>Počet členů<? echo ($aktualni_rok==$rok_registrace)?"":" (".$rok_registrace.")";?>:</td><td class="left" width=340><? echo $strediska_data['pocet'][$aktualni_stredisko]; ?></td>
		<td rowspan="2"><p class="normal">
		V dolním grafu je zobrazeno, jak si středisko vede v kritériích nezávislých na jeho velikosti (např. administrativní úkoly). V horním grafu je zobrazeno, jak si vede středisko v kritériích závislých na jeho velikosti (např. počet táborových děťodnů) přepočteno na deset členů střediska.
		</p>
		</td>
		</tr>
		<tr><td class="right">Celkový počet bodů:</td><td class="left"><? echo round($strediska_data['celkem_bodu'][$aktualni_stredisko],2); ?></td></tr>
		</table>
		<br>	

    		<table style="border-top: solid 2px #5E5E5E;" width=100%>
    		<tr><td style="vertical-align: top">
	    		<div id="chartdiv<? echo count($KHMS_data)-1; ?>" style="width: 500px; height: 320px;"></div>
	    	</td>
	    	<td style="vertical-align: top; padding-top: 10px; padding-left: 10px;">
		    	<!--<h3>Z čeho se tato čísla skládají:</h3>//-->
		    	<br>
		    	<table style="border: solid 0px #000000; text-align: center">
		    	<tr style="font-weight: bold; border-bottom: solid 1px #000000"><td width="270px" style="text-align: left">Oblast kritérií</td><td width="70px">Hodnota</td><td width="100px">Body (na 10 členů)</td><td width="100px">Pořadí v okrese (z <? echo  $pocet_stredisek; ?>)</td><!--<td width="200px"></td>//--></tr>
		    	<? for ($j=1;$j<=count($KHMS_data)-2;$j++):
		    		if ($typ_grafu[$j]=="na_stredisko") { break; } 
		    		echo "<tr height=\"1px\"><td class=\"radek_maly_graf\" colspan=4></td></tr>";		    		
		    		echo "<tr height=\"20px\"><td style=\"text-align: left; height: 30px\"><a href=\"javascript: zobraz('".$j."')\" id=\"odkaz".$j."\"><img src=\"images/detail-mi.gif\" id=\"sipka".$j."\"> ".$nazev_grafu[$j]."</a></td><td>".round($KHMS_data[$j]['pro_graf'][$aktualni_stredisko],2).$procenta[$j]."</td><td>".round($KHMS_data[$j]['body_pro_tabulku'][$aktualni_stredisko],2)."</td><td".$rank_style[$j][$aktualni_stredisko]."><b>".$rank[$j][$aktualni_stredisko]."</b></td><!--<td>".$rank_text[$j][$aktualni_stredisko]."</td>//--></tr>";
		    		?>
		    		<tr><td colspan=4>
		    			<div id="graf<? echo $j; ?>" style="height: 0px; overflow: hidden">
		    			<table>
			       		<tr><td>
			    		<div id="chartdiv<? echo $j; ?>" style="width: 380px; height: 250px;"></div>
			    		</td>
			    		<td>
			    		<?
			    		echo "<p class=\"normal\">".$nazev_grafu2[$j].": ".round($KHMS_data[$j]['stav'][$aktualni_stredisko],2);
			    		echo "<br>za to bodů: ".round($KHMS_data[$j]['body'][$aktualni_stredisko],2);
			    		echo "<br><br>ztráta na prvního: ".$KHMS_data[$j]['ztrata_na_prvniho_stav'][$aktualni_stredisko];
			    		echo "<br>za to by bylo bodů: +".$KHMS_data[$j]['ztrata_na_prvniho_body'][$aktualni_stredisko];
			    		?>
			    		</td></tr>
			    		</table>
			    		</div>
		    		</td></tr>
		    		
		    		<?
		    	endfor; 
		    	echo "<tr height=\"10px\"><td class=\"radek_maly_graf\" colspan=4></td></tr>";
		    	echo "<tr style=\"font-weight: bold\"><td style=\"text-align: left\">Celkem</td><td></td><td>".round($KHMS_data[count($KHMS_data)-1]['body_pro_tabulku'][$aktualni_stredisko],2)."</td><td".$rank_style[count($KHMS_data)-1][$aktualni_stredisko].">".$rank[count($KHMS_data)-1][$aktualni_stredisko]."</td></tr>";
		    	?>
		    	</table>
		    	<br>
	    	</td></tr>
	    	</table>

	       	
			<table style="border-top: solid 2px #5E5E5E; border-bottom: solid 2px #5E5E5E;" width=100%>
    		<tr><td style="vertical-align: top">
	       		<div id="chartdiv<? echo count($KHMS_data); ?>" style="width: 500px; height: 300px;"></div>
	    	</td>
	    	<td style="vertical-align: top; padding-top: 10px; padding-left: 10px;">
		       	<!--<h3>Z čeho se tato čísla skládají:</h3>//-->
		    	<br>	
		       	<table style="border: solid 0px #000000; text-align: center">
		    	<tr style="font-weight: bold; border-bottom: solid 1px #000000"><td width="270px" style="text-align: left">Oblast kritérií</td><td width="70px">Hodnota</td><td width="100px">Body</td><td width="100px">Pořadí v okrese (z <? echo  $pocet_stredisek; ?>)</td><!--<td width="200px"></td>//--></tr>
		    	<?
		    	for ($j=$j;$j<=count($KHMS_data)-2;$j++):
		    	
		    		echo "<tr height=\"1px\"><td class=\"radek_maly_graf\" colspan=4></td></tr>";		    		
		    		echo "<tr id=\"radek".$j."\"><td style=\"text-align: left; height: 30px\"><a href=\"javascript: zobraz('".$j."')\" id=\"odkaz".$j."\"><img src=\"images/detail-mi.gif\" id=\"sipka".$j."\"> ".$nazev_grafu[$j]."</a></td><td>".round($KHMS_data[$j]['pro_graf'][$aktualni_stredisko],2).$procenta[$j]."</td><td>".round($KHMS_data[$j]['body_pro_tabulku'][$aktualni_stredisko],2)."</td><td".$rank_style[$j][$aktualni_stredisko]."><b>".$rank[$j][$aktualni_stredisko]."</b></td><!--<td>".$rank_text[$j][$aktualni_stredisko]."</td>//--></tr>";
		    		?>
		    		<tr><td colspan=4>
		    			<div id="graf<? echo $j; ?>" style="height: 0px; overflow: hidden">
		    			<table>
			       		<tr><td>
			    		<div id="chartdiv<? echo $j; ?>" style="width: 380px; height: 250px;"></div>
			    		</td>
			    		<td>
			    		<?
			    		echo "<p class=\"normal\">".$nazev_grafu2[$j].": ".round($KHMS_data[$j]['stav'][$aktualni_stredisko],2);
			    		echo "<br>za to bodů: ".round($KHMS_data[$j]['body'][$aktualni_stredisko],2);
			    		echo "<br><br>ztráta na prvního: ".$KHMS_data[$j]['ztrata_na_prvniho_stav'][$aktualni_stredisko];
			    		echo "<br>za to by bylo bodů: +".$KHMS_data[$j]['ztrata_na_prvniho_body'][$aktualni_stredisko];
			    		?>
			    		</td></tr>
			    		</table>
			    		</div>
		    		</td></tr>
		    		
		    		<?
		    	
		    	endfor;
		    	echo "<tr height=\"10px\"><td class=\"radek_maly_graf\" colspan=4></td></tr>"; 
		    	echo "<tr style=\"font-weight: bold\"><td style=\"text-align: left\">Celkem</td><td></td><td>".round($KHMS_data[count($KHMS_data)]['body_pro_tabulku'][$aktualni_stredisko],2)."</td><td".$rank_style[count($KHMS_data)][$aktualni_stredisko].">".$rank[count($KHMS_data)][$aktualni_stredisko]."</td></tr>";
		    	?>
		    	</table>
		    	<br>
	    	</td></tr>
	    	</table>
	    	
	    	<?
	    	//Nalezeni oblasti s nejvetsi ztratou na prvni stredisko
	    	foreach ($KHMS_data as $key => $value) {
		    	if (array_key_exists('ztrata_na_prvniho_body', $KHMS_data[$key]) && is_Array($KHMS_data[$key]['ztrata_na_prvniho_body'])):
		    	foreach ($KHMS_data[$key]['ztrata_na_prvniho_body'] as $key2 => $value2) {
			    	
			    	 if ($key2==$aktualni_stredisko):
				    	 $polozky_ke_zlepseni[$key]=$value2;
				    	 //$polozky_ke_zlepseni['polozka'][]=$key;
				    	 
			    	 endif;
		    	 }
		    	 endif;
	    	 }
	    	 ?>
	    	 <div style="width: 610px; margin: 0 auto;">
	    	 <h2>Oblasti s největším potenciálem:</h2> 

	    	 <table style="width: 100%">
	    	 <tr>
   	 	     <td width=55%></td>
   	 	     <td width=20%></td>
   	 	     <td width=25%></td>
   	 	     </tr>	

	    	 <?
	    	 $style_array=array("font-size: 14px; color: #FF0000;", "font-size: 13px; color: #E46D0A;", "font-size: 12px; color: #FFC000;");
	    	 ARSort($polozky_ke_zlepseni);
	    	 $i=0;
	    	 foreach ($polozky_ke_zlepseni as $key => $value) if ($i < 3) {
		    	if (($ukaz_doporuceni[$key]=="ano")&&($value>0)) {
		    	 	echo "<tr style=\"font-weight: bold; ".$style_array[$i]."\"><td style=\"text-align: right; padding-right: 10px; height: 25px\">".$nazev_grafu[$key].":</td><td style=\"text-align: left; padding-left: 10px\"> +".$value."</td><td class=\"left\" style=\"font-weight: normal; font-size: 12px\"><a href=\"#odkaz".$key."\" onclick=\"zobraz_pouze('".$key."')\" >detaily&nbsp;<b>&uarr;</b></a></td></tr>";
		    	 	$i++;
	    	 	}
	    	 }
			    	
	    	?>
	    	</table>
	    	<!--zobrazit vše <img src="images/detail-mi2.gif">//-->
	    	<br>
	    	Jde o oblasti (vyjma: <?
	 	     $druhy_vyskyt="ne";
	 	     foreach ($nazev_grafu  as $key => $value) {
		 	    if ($ukaz_doporuceni[$key]=="ne") {
		 	     	echo ($druhy_vyskyt=="ano")?", ".$value:$value;
		 	     	$druhy_vyskyt="ano";
	 	     	}
		 	     	
	 	     } 
	 	     ?>), ve kterých by středisko získalo nejvíc bodů, kdyby se vyrovnalo středisku, jež je v dané oblasti první v okrese.
	 	     <?/*
	 	     	$seznam_kriteriis=mysql_query("SELECT * FROM `KHMS_kriteria` WHERE termin IS NOT NULL ORDER BY termin");
	 	     	while($seznam_kriterii=mysql_fetch_row($seznam_kriteriis)):
	 	     		$terminy['nazev'][]=$seznam_kriterii[1];
	 	     		$terminy['termin'][]=$seznam_kriterii[5];
	 	     		
	 	     		echo $seznam_kriterii[5]-gregoriantojd(Date("n"),Date("j"),Date("Y"));
	 	     		$terminy['vzdalenost'][]
	 	     		//echo jdtogregorian($seznam_kriterii[5]);
	 	     		//echo jdtogregorian(gregoriantojd(Date("n"),Date("j"),Date("Y")));
	 	     	endwhile;
	 	     	foreach ($seznam_kriterii as $key => $value) {
		 	     	
	 	     	}*/
	 	     ?>
	 	     </div>
	 	     </br>
      </div>


    	</div>