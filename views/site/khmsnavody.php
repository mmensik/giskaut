﻿<?php
/* @var $this yii\web\View */
$this->title = 'GISkaut';

use app\models\Unit;
use app\models\Registration;
use app\models\Person;
use app\models\Klubovna;
use app\widgets\GoogleCharts;
use app\widgets\GoogleMap;

use yii\helpers\Html;
use yii\widgets\ListView;

use app\models\FunkcePersonUnit;
use yii\data\ArrayDataProvider;



    $navod_nadpis=Array(1=>
    "Včasné odevzdání registrace",
	"Včasné zaplacení registrace",
	"Včasné odevzdání hospodářského výkazu",
	"Včasné vyúčtování poskytnuté finanční dotace",
	"Včasné odevzdání zprávy o inventarizaci majetku",
	"Včasné odevzdání hlášení táborů",
	"Včasné odevzdání skutečnosti táborů",
	"Včasné odevzdání zprávy RK a provedení v minulém roce min. jedné revize",
	"Středisko do konce května následujícího roku odevzdalo výroční zprávu",
	"Středisko (včetně oddílů) nemá při registraci žádnou výjimku z organizačního řádu",
	"100% táborů pořádaných střediskem do jednoho měsíce od skončení odevzdaly ve SkautISu své hodnocení kvality",
	"Středisko vložilo do SkautISu v posledních třech letech hodnocení kvality střediska");
    
	
	$navod_termin=Array(1=>
	"31.1.",
	"28.2.",
	"15.4.",
	"listopad - prosinec (upřesnění přijde vždy mailem)",
	"30.4.",
	"31.5.",
	"30.9.",
	"30.4.",
	"31.5.",
	"při registraci",
	"30.9.",
	"15.10.");
	
	
    $navod_text=Array(1=>
    "Návod na registraci najdete přímo v nápovědě SkautISu <a target=\"_blank\" href=\"http://is.skaut.cz/napoveda/Registrace.ashx\">zde</a>.",
    "Po odevzdání registraci je nutné zaplatit registrační poplatky na okresní účet č. 242686068/0300. Částku, kterou je potřeba na okres poslat zjistíte opět ve <a target=\"_blank\" href=\"http://is.skaut.cz\">SkautISu</a> na záložce &quot;Přehled odvodů&quot; příslušné Registrace.  Tedy nejprve vyberete svoje středisko, tady kliknete na záložku &quot;Registrace&quot; a ukáže se vám seznam registrací podle jednotlivých let. Zde už přímo u příslušného roku můžete kliknout na odkaz &quot;Přehled odvodů&quot; nebo kliknete na příslušný rok a poté na záložku &quot;Přehled odvodů&quot;.<p>A hned na začátku je &quot;Suma odvodů pro nadřízenou jednotku&quot;. Vůbec nic složitého.",
    "Hospodářský výkaz se odevzdává ve <a target=\"_blank\" href=\"http://is.skaut.cz/\">SkautISu</a>. Po přihlášení vyberte “Moje jednotka” a potom záložku Hospodářské výkazy, kde výkaz založíte, vyplníte a odevzdáte. Odevzdává se do 15. dubna, tato povinnost vyplývá ze směrnice pro registraci pro daný rok.",
    "Tento úkol se týká tzv. provozní dotace, kterou u nás čerpá každé středisko. Provozní dotace se pak vyúčtovávají souhrnně do formuláře for_vyuctovani_sumar, jednotlivé provozní náklady se rozepisují do for_dot_v2 a pokud čerpáte dotaci na krátkodobé akce, tak na každou akci zvlášť použijete for_vyuctovani_dotace. Nejprve je třeba zaslat vyúčtování elektronicky a poté papírově a v této souvislosti je posílána mailem řada upozornění včetní termínů odevzdání. Všechny potřebné formuláře jsou přiloženy.",
    "Způsob inventarizace a všechny související věci, které je pro a po inventarizaci potřeba udělat, popisuje přiložená Směrnice k inventarizaci Junáka a Metodika k inventarizaci.<p>Ve zkratce řečeno: Vedoucí střediska jmenuje nejméně dvoučlennou inventarizační komisi (jejím členem nesmí být vedoucí střediska, hospodář nebo správce majetku - to ale neznamená, že u inventury nemůžou být k ruce) a tato komise v období mezi 1.9. až 31.1. provede inventuru. To znamená, že vyrazí do střediskového skladu majetku a zkontroluje, zda tam je skutečně to, co je uvedeno v evidenci, kterou vede hospodář střediska nebo správce majetku.<p>Poté inventarizační komise vypracuje inventarizační zápis (jeho náležitosti jsou uvedeny v bodě 7.1 přiložené Směrnice o inventarizaci, vyplněný vzor najdete v přiložené metodice a příslušný formulář je také v příloze) a tento zápis vloží vedoucí střediska nejpozději do 30. 4. do příslušné střediskové složky v okresním úložišti (<a target=\"_blank\" href=\"http://drive.google.com/drive/folders/0B6xRphfsy34YZ1lSQ0ZVMGMydlk\">tady</a>). Žádná velká věda.",
    "Nejprve je třeba tábor ve SkautISu založit (to zpravidla dělá vedoucí tábora). Takže po přihlášení do <a target=\"_blank\" href=\"http://is.skaut.cz/\">SkautISu</a> stačí vybrat záložku &quot;Akce&quot;, potom pod ní záložku &quot;Tábory&quot; a tady kliknout na Nový tábor. Zde je potřeba vyplnit všechny náležitosti, na konci tábor schválit jako Vedoucí tábora a poté jej někdo musí schválit z pozice střediska (opět záložka &quot;Akce&quot;, pak  &quot;Tábory&quot;, tady vybrat příslušný tábor a na záložce &quot;Uzavření a odevzdání&quot; kliknout vpravo dole na &quot;Schválit&quot;). A je to.",
    "Odevzdání skutečnosti táborů opět probíhá ve SkautISu. Tady stačí vybrat záložku &quot;Akce&quot;, potom pod ní záložku &quot;Tábory&quot;, zde vybrat příslušný tábor a tady na záložkách &quot;Účastníci a počty tábořících&quot; a &quot;Rozpočet&quot; zadat skutečné údaje o táboře. Jakmile tyto údaje vyplníte, je potřeba na kartě &quot;Uzavření a odevzdání&quot; skutečnost tábora odevzdat. Hotovo dvacet.",
    "Provádění revizí a vypracování revizní zprávy je čistě v režii revizní komise střediska. Ta by měla každý rok vypracovat roční zprávu o činnosti. Její náležitosti, stejně jako průběh kontroly jsou uvedeny v přiloženém Revizním řádu Junáka. Pro získání KHMS bodů stačí, když revizní komise provede alespoň jednu revizi a její roční zprávu pak někdo za středisko vloží nejpozději do 30. 4. do příslušné střediskové složky v okresním úložišti (<a target=\"_blank\" href=\"http://drive.google.com/drive/folders/0B6xRphfsy34YZ1lSQ0ZVMGMydlk\">tady</a>).<p>V příloze najdete vzor, který obsahuje řadu zajímavých inspirací, co kontrolovat. Ale nelekněte se jeho rozsahu a nebojte se klidně všechno umazat - je to výhoda, budete spíše mazat a na nic podstatného nezapomenete.",
    "Asi nejlepší je řídit se radami a šablonami, které každoročně vydává Ústředí Junáka. Při jejich používání nemůžete udělat chybu. <a target=\"_blank\" href=\"http://krizovatka.skaut.cz/zpravodajstvi/1774-vlastni-vyrocka\">Zde</a> naleznete přehledný článek na křižovatce a dva vzory výroční zprávy najdete v příloze.<p>Je dobré kontrolovat Křižovatku a používat šablony Ústředí, které se každý rok mění (takže jsou originální) a navíc je v nich vše podstatné, takže na nic nezapomenete!",
    "Na výjimky vás upozorní <a target=\"_blank\" href=\"http://is.skaut.cz/\">SkautIS</a> při zadávání registrace, takže tady se o tom není potřeba nijak dlouze rozepisovat, o všem se ve SkautISu dozvíte. Pro zájemce o detailní popis toho, co je výjimka a co není, je přiložen organizační řád.",
    "Samotné hodnocení kvality tábora probíhá obvykle někde u Kofoly po táboře nebo na nějaké potáborové oddílové radě s partou lidí, kteří tábor připravovali. Je ale dobré se na to připravit. Hodnocení kvality je nejprve potřeba založit ve SkautISu. To zde udělá vedoucí tábora a jde to až poté, co je tábor schválený střediskem a už začal.<p>Po přihlášení do <a target=\"_blank\" href=\"http://is.skaut.cz/\">SkautISu</a> je potřeba pracovat jako vedoucí/administrátor nebo aktivní činovník střediska nebo vedoucí/administrátor oddílu, který tábor pořádá (to se ve SkautISu nastavuje vpravo nahoře). Potom kliknout na záložku “Kvalita” a zde vybrat “Nové hodnocení”. Poté je potřeba zvolit “Typ hodnocení” jako “Akce”, vybrat příslušný tábor a vpravo dole kliknout na “Založit”. Jakmile je hodnocení založeno, můžete si zde kliknutím na “Hodnocení kvality (XLSX)” jednak stáhnout a vytisknout otázky k hodnocení, které pak vezmete na tu oddílovou radu, kde provedete hodnocení. Ale hlavně zde pak můžete zadávat už hotové hodnocení vyplněním příslušného elektronického dotazníku, které je po dokončení nutno odevzdat kliknutím na “Odevzdat”. Jestli je vaše táborová rada zvyklá na SkautIS, můžete celé hodnocení urychlit tzv. individuálním hodnocením (jak na něj <a target=\"_blank\" href=\"http://is.skaut.cz/napoveda/GetFile.aspx?File=%2fdokumenty%2findividualni_hodnoceni_kvality.pdf&Provider=ScrewTurn.Wiki.FilesStorageProvider\">zde</a>), ale jinak si tím nelamte hlavu.<p>Pokud se ale nechcete hned složitostmi ve SkautISu zdržovat, nechte to na potom a pro začátek najdete v příloze stručné shrnutí, jak hodnocení kvality tábora zpracovat včetně otázek, kterými se tábor hodnotí. Tento soubor si můžete vytisknout a vzít s sebou na tábor, na jehož konci už rovnou můžete začít provádět jeho vyhodnocení (aniž byste museli k počítači, tam něco zakládat a tisknout - do SkautISu to můžete naklikat později).",
    "Hodnocení kvality střediska probíhá úplně stejně jako hodnocení kvality tábora (viz <a href=\"#nadpis11\">předchozí bod</a>), pouze s tím rozdílem, že jej můžete ve SkautISu založit kdykoliv, a může to udělat jen ten, kdo má roli vedoucí/administrátor střediska. Hodnocení provádí středisková rada a stručné shrnutí včetně otázek hodnocení střediska, se kterým se do toho můžete rovnou pustit, naleznete v příloze."
    );
    
    $navod_priloha['nazev']=Array(1=>
    "",
    "",
    "",
    Array(1=>"for_vyuctovani_dotace_na_akci_N","for_dot_v2_N","for_vyuctovani_sumar_N"),
    Array(1=>"Směrnice k inventarizaci", "Metodika k inventarizaci", "Formulář zprávy o inventarizaci"),
    "",
    "",
    Array(1=>"Revizní řád", "Vzor revizní zprávy"),
    Array(1=>"Šablona výroční zprávy č. 1", "Šablona výroční zprávy č. 2"),
    Array(1=>"Organizační řád"),
    Array(1=>"Dotazník HK tábor"),
    Array(1=>"Dotazník HK středisko")
    );
    
    $navod_priloha['link']=Array(1=>
    "",
    "",
    "",
    Array(1=>"for_vyuctovani_dotace_na_akci_N.xls","for_dot_v2_N.xls","for_vyuctovani_sumar_N.xls"),
    Array(1=>"smernice_k_inventarizaci.pdf", "metodika_7_inventarizace.pdf", "formular_ke_smernici_o_inventarizaci.xls"),
    "",
    "",
    Array(1=>"rad_revizni.pdf", "1416400120_Vzorov_revizn zprava_podvojko_A.docx"),
    Array(1=>"vyrocni_zpava_sablona-2.docx","vyrocni_zpava_sablona-1.docx"),
    Array(1=>"rad_organizacni_2014-11-29_final.pdf"),
    Array(1=>"Dotaznik-HK-tabor-2012-1.pdf"),
    Array(1=>"Dotaznik-HK-stredisko-2012-1.pdf")
    );
    
    function get_file_type($file_name) {
	    
		if (Substr($file_name,strlen($file_name)-4,4)==".pdf"):
			$iconm="iconm_pdf.png";
		elseif (Substr($file_name,strlen($file_name)-4,4)==(".doc")||Substr($file_name,strlen($file_name)-4,4)==("docx")):
			$iconm="iconm_doc.png";	
		elseif (Substr($file_name,strlen($file_name)-4,4)==(".xls")||Substr($file_name,strlen($file_name)-4,4)==("xlsx")):
			$iconm="iconm_xls.png";
		else:
			$iconm="iconm_file.png";
		endif;  
		return $iconm;
    }
    
    ?>
    <style>
    p {margin-left: 30px; margin-right: 40px; margin-bottom: 0px; margin-top: 8px; text-align: justify; }
    </style>
    <?
    echo "<div class=\"main\">";
    
 	echo "<h1>Jak získat body za KHMS</h1>";
    
 	//obsah
    echo "<h2 id=\"obsah\">Obsah:</h2><ul>";
    foreach ($navod_nadpis as $key => $value) {
		echo "<li><a href=\"#nadpis".$key."\">".$value."</a></li>";
	}
	echo "</ul>";
    
	//stranka
	foreach ($navod_nadpis as $key => $value) {
		echo "<h2 id=\"nadpis".$key."\">".$value." <a href=\"#obsah\">&uarr;</a></h2>";
		echo (Empty($navod_termin[$key]))?"":"<p>Termín: ".$navod_termin[$key]."</p>";
		echo "<p>".$navod_text[$key]."</p>";
		echo (Empty($navod_priloha['nazev'][$key]))?"":"<p><i>Přílohy:<br>";
		if (Is_Array($navod_priloha['nazev'][$key])):
		foreach ($navod_priloha['nazev'][$key] as $key2 => $value2) {
			echo "<a class=\"a_priloha\" target=\"_blank\" href=\"files/navody_prilohy/".$navod_priloha['link'][$key][$key2]."\">".$value2." <img src=\"images/".get_file_type($navod_priloha['link'][$key][$key2])."\"></a><br>";
		}
		endif;
		echo (Empty($navod_priloha['nazev'][$key]))?"":"</i></p>";
	}
	
    ?>
    </div>