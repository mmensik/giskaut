﻿<?php
/* @var $this yii\web\View */
$this->title = 'GISkaut';

use app\models\Unit;
use app\models\Registration;
use app\models\Person;
use app\models\Klubovna;
use app\widgets\GoogleCharts;
use app\widgets\GoogleMap;

use yii\helpers\Html;
use yii\widgets\ListView;

use app\models\FunkcePersonUnit;
use yii\data\ArrayDataProvider;


?>
<h1>Okresní rada Junáka</h1>
<p>Okresní rada Junáka (ORJ) Šumperk je nejvyšším řídícím orgánem skautského okresu Šumperk (pod který spadá i okres Jeseník). Volení členové ORJ Šumperk byli zvoleni na okresním sněmu Junáka 8. 12. 2013 na tříleté období. Členové ORJ jsou dále vedoucí středisek spadajících do skautského okresu Šumperk. Úkolem ORJ je naplňovat smysl skautského okresu, tedy podporovat skautskou výchovu dětí a mládeže v dané oblasti, zvyšovat její úroveň a kvalitu, poskytovat zázemí pro činnost ve střediscích a podporovat jejich vedení a sdružovat, organizovat a řídit střediska, v příslušném okrese registrovaná.</p>
<p>Chceme být užiteční skautingu v okresech Šumperk a Jeseník. Vycházíme z toho, že tu nejsou střediska a oddíly pro nás, ale my pro ně. Chceme se ale pouštět jen do toho, na co máme síly, abychom dělali s radostí to, co nás baví.</p>
<h2>Kontakt</h2>
<div class="col-md-6">
	<p>Junák - český skaut, okres Šumperk, z.s.<br>
	náměstí Jana Zajíce 11<br>
	787 01 Šumperk<br>
	orjspk@skaut.cz (kompletní <? echo Html::a("adresář kontaktů","https://docs.google.com/spreadsheets/d/1NXHjlN4QbnFXatLEIb3EEn20RQSGdlbXSuyBQDifLgs/edit#gid=0",['target' => '_blank']); ?>)</p>
</div>
<div class="col-md-6">
	<dl class="dl-horizontal">
	<dt>&nbsp;</dt><dd>&nbsp;</dd>
	<dt>IČO:</dt><dd>603 41 921</dd>
	<dt>evidenční číslo:</dt><dd> 715</dd>
	<dt>číslo účtu:</dt><dd>242686068/0300</dd>
	</dl>
</div>
<h2>Lidé</h2>

<?php
	$dataProvider = new ArrayDataProvider([
			'allModels' => $clenoveRady,
		]);

	echo ListView::widget([
			'dataProvider' => $dataProvider,
			'itemView' => '_clenRady',
			'separator' => '',
			'layout' => '{items}',
			'options' => ['class' => 'row']
		]);
?>