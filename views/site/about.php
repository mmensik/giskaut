﻿ <?php
/* @var $this yii\web\View */
$this->title = 'GISkaut';

use app\models\Unit;
use app\models\Registration;
use app\models\Person;
use app\models\Klubovna;
use app\widgets\GoogleCharts;
use app\widgets\GoogleMap;

use yii\helpers\Html;
?>

<div class="col-md-7">


      <div class="titulni_box" id="box_aktuality">
      <p class="box_nadpis">Aktuální dění</p>
      <div class="box_doplnkove_info">novinky, úkoly, pozvánky</div>
      <br>

      <?php

      $funkce_all = $model->funkcePersonUnits;

      foreach ($funkce_all as $funkce) {
        //echo $funkce->funkce->name . ' - ' . $funkce->person->name . ' ' . $funkce->person->surname . '<br>';
      }

 ?>
      <img src="images/sipka_main_page.png" class="sipka">
      </div>
      
      
      <br>
      <div class="titulni_box" id="box_onas">
      <p class="box_nadpis">Okresní rada</p>
      <div class="box_doplnkove_info">kontakt, lidé, poslání</div>
      <p>Junák - český skaut, okres Šumperk, z. s.<br>
      náměstí Jana Zajíce 11, 787 01 Šumperk<br>
      IČO: 603 41 921</p>
      <a href="?r=site/orj"><img src="images/sipka_main_page.png" class="sipka"></a>
      </div>
      <br>
</div>

<div class="col-md-5">
      <div class="titulni_box" id="box_statistiky">
      <p class="box_nadpis">Info o nás</p>
      <div class="box_doplnkove_info">kdo, kde, kontakty</div>
      <div class="mapa_main_page" style="margin-left:auto; margin-right: auto;  width: 80%">
   <?php 


GoogleMap::begin([
                        'id' => 'map-canvas',
                        'width' => '100%',
                        'height' => '250px'
                        ]) ?>
<script>
      function initialize() {
        var activeInfo;
        var heatmapData = [
        <?php 

        $members = Person::find()->all();
        $latSum = 0;
        $count = 0;
        $lngSum = 0; 
        foreach ($members as $member)
        {
          if (!$member->lat or !$member->lng or $member->lat < 40 or $member->lng < 10) continue;
          $count++;
          $latSum += $member->lat;
          $lngSum += $member->lng;
        ?>
            new google.maps.LatLng(<?= $member->lat ?> ,<?= $member->lng ?>),
            <?
        }
        ?>
        ];
        var mapCenter = new google.maps.LatLng( <?= $latSum / ($count ? $count : 1) ?>, <?= $lngSum / ($count ? $count : 1) ?>);
        var mapOptions = {
          //center: { lat: <?= $latSum / ($count ? $count : 1) ?>, lng: <?= $lngSum / ($count ? $count : 1) ?>},
          center: { lat: 49.933765, lng: 16.878613},
          zoom: 11,
          //maxZoom: 13,
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);
        var heatmap = new google.maps.visualization.HeatmapLayer({
          data: heatmapData,
          //disipating: true.
          radius: 30,
        });
        heatmap.setMap(map);

        google.maps.event.addListener(map, 'zoom_changed', function() {
          var zoomLevel = map.getZoom();
          if (zoomLevel > 13)
          {
            heatmap.setMap(null);
          } else {
            heatmap.setMap(map);
          }
        });


        <?php
        $klubovny = Klubovna::find()->all();

        foreach($klubovny as $klubovna)
        {
            if (!$klubovna->lat or !$klubovna->lng or $klubovna->lat < 40 or $klubovna->lng < 10) continue;
            $units = $klubovna->GetAllUnits();
        ?>
            var marker<?= $klubovna->id ?> = new google.maps.Marker({
                position: new google.maps.LatLng(<?= $klubovna->lat ?> ,<?= $klubovna->lng ?>),
                map: map,
                title: '<?= $klubovna->name ?>',
                icon: '<?= Unit::ContainsCathegory($units, "Středisko") ? "images/stredisko.png" : "images/oddil.png" ?>',
            });

            var contentString = '<strong><?= $klubovna->name ?></strong>'
                              + '<ul>'
            <?php
            foreach ($units as $unit) {
              if ($unit->type == 'Družina') continue;
            ?>
                              + '<li><?= $unit->type ?> <?= Html::a($unit->name, ["chart/zakladna-vyvoj", "id"=>$unit->id]) ?></li>' 
            <?php
            }
            ?>
                              + '</ul>'

            var infoWindow<?= $klubovna->id ?> = new google.maps.InfoWindow({
               content: contentString,
            });

            google.maps.event.addListener(marker<?= $klubovna->id ?>, 'click', function() {
                if (activeInfo) activeInfo.close();
               infoWindow<?= $klubovna->id ?>.open(map,marker<?= $klubovna->id ?>);
               activeInfo = infoWindow<?= $klubovna->id ?>;
            });
        <?
        }
        
        ?>

      }
      google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php GoogleMap::end() ?>
    </div>
    <img src="images/sipka_main_page.png" class="sipka">
      </div>
      
      
      <br>
      <div class="titulni_box" id="box_khms">
      <p class="box_nadpis">Statistiky</p>
      <div class="box_doplnkove_info">grafy, čísla, tabulky</div>
      <br>
      <?= $this->render('/chart/_zakladna_pohlavi', ['data'=>$model->zakladnaByPohlavi]); ?>
      <img src="images/sipka_main_page.png" class="sipka">
      </div>
      
      
      <br>
      <div class="titulni_box" id="box_orj">
      <p class="box_nadpis">Kritéria hodnocení a motivace středisek</p>
      <br>
      <img src="images/sipka_main_page.png" class="sipka">
      </div>

</div>

