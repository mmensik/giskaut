
<?php 

    use app\assets\D3Asset;
    use app\models\Unit;

    use yii\bootstrap\Modal;

    D3Asset::register($this);

    $dataset = [
        'title' => 'ORJ',
        'subUnits' => []
    ];


    $strediska = Unit::find()->where(['type'=>'Středisko'])->orWhere(['type'=>'Okres'])->andWhere(['not', ['id'=>[2,85]]])->OrderBy('Code')->all();

    foreach ($strediska as $stredisko)
    {
        $s = [
            'title' => $stredisko->name,
            'subUnits' => []
        ];

        foreach ($stredisko->units as $oddil)
        {
            $s['subUnits'][] = ['title' => $oddil->name];
         //   echo Html::a($oddil->name, [$action, 'id' => $oddil->id], ['class' => 'list-group-item list-group-item-info '. 
          //                                      ($activeOddilId== $oddil->id ? 'active' : '')]);
        }

        $dataset['subUnits'][] = $s;
    }

  

?>
<br/><br/>
<?php

Modal::begin([
    'header' => '<h2>Hello world</h2>',
    'toggleButton' => [
        'tag'   => 'a',
        'label' => 't',
        //'href'  => '',
        ],
    ]);

    echo 'Say hello...';

    Modal::end();
?>

<svg id="graph" viewBox="0 0 960 500" preserveAspectRatio="xMidYMid">

<script>
var dataset =  <?php echo json_encode($dataset); ?>;

var width = 960,
    height = 500;

var x = d3.scale.linear()
    .range([0, width]);

var y = d3.scale.linear()
    .range([0, height]);

var color = d3.scale.category20c();

var partition = d3.layout.partition()
    .children(function(d) { return d['subUnits']; })
    .value(function(d) { return 1; });

var svg = d3.select("svg");

var data = partition(dataset);

var rect = svg.selectAll("rect").data(data).enter()
 .append("svg:rect")
 .attr("x", function(d) { return x(d.x); })
      .attr("y", function(d) { return y(d.y); })
      .attr("width", function(d) { return x(d.dx); })
      .attr("height", function(d) { return y(d.dy); })
      .attr("fill", function(d) { return color(d.children ? d.title : d.parent.title); })
 
 .style("cursor", "pointer")
 .on("click", clicked);

var fo = svg.selectAll("foreignObject").data(data).enter()
 .append("svg:foreignObject")
 .attr("x", function(d) { return x(d.x); })
      .attr("y", function(d) { return y(d.y); })
      .attr("width", function(d) { return x(d.dx); })
      .attr("height", function(d) { return y(d.dy); })
      
 .style("cursor", "pointer")
 .text(function(d) { return d.title })
 .on("click", clicked);


function clicked(d) {
  x.domain([d.x, d.x + d.dx]);
  y.domain([d.y, 1]).range([d.y ? 20 : 0, height]);

  rect.transition()
      .duration(750)
      .attr("x", function(d) { return x(d.x); })
      .attr("y", function(d) { return y(d.y); })
      .attr("width", function(d) { return x(d.x + d.dx) - x(d.x); })
      .attr("height", function(d) { return y(d.y + d.dy) - y(d.y); });

  fo.transition()
      .duration(750)
      .attr("x", function(d) { return x(d.x); })
      .attr("y", function(d) { return y(d.y); })
      .attr("width", function(d) { return x(d.x + d.dx) - x(d.x); })
      .attr("height", function(d) { return y(d.y + d.dy) - y(d.y); });
}
</script>

