<div class="col-sm-6 col-md-4">
<div class="thumbnail">
  <? if (File_Exists("images/lide/".$model['osoba']->id.".gif")): ?>
  <img src="images/lide/<?= $model['osoba']->id ?>.gif" class="portret">
  <? elseif (File_Exists("images/lide/".$model['osoba']->id.".jpg")): ?>
  <img src="images/lide/<?= $model['osoba']->id ?>.jpg" class="portret">
  <? endif; ?>
  <div class="caption">
    <h3><?= /*$model['osoba']->id.'. '.*/$model['osoba']->fullName ?></h3>
    <p>
    <ul class="list-unstyled">
    <?php foreach ($model['funkce'] as $f) { ?>
      <li><strong><?= $f->funkce->name ?></strong> <?= (($f->funkce->id)==1)?"(".$f->unit->name.")":""; ?></li>
    <?php } ?>
    </ul>
    </p>
  </div>
 </div>
 </div>