﻿ <?php
/* @var $this yii\web\View */
$this->title = 'GISkaut';

use app\models\Unit;
use app\models\Ukoly;
use app\models\Registration;
use app\models\Person;
use app\models\Klubovna;
use app\models\Document;
use app\models\Event;
use app\models\News;
use app\widgets\GoogleCharts;
use app\widgets\GoogleMap;

use yii\helpers\Html;
use yii\bootstrap\Modal;

?>

<div class="col-md-7">


      <div class="titulni_box" id="box_aktuality">
      <p class="box_nadpis">Aktuální dění</p>
      <div class="box_doplnkove_info">novinky, úkoly, pozvánky</div>

      <?php
      function date_stamp ($date,$sign,$dist) {
	      $time_stamp=' ';
	      $time_stamp.=($sign>0)?'(bude ':'(vloženo ';
	      $time_stamp.=Yii::$app->formatter->asDate($date, 'php:d.m.Y') . ', ';
	      //$time_stamp.=($sign>0)?', <font color=red>':', <font color=green>';
	      if ($dist==0):
	      	$time_stamp.='Dnes)';
	      elseif (($dist==1)&&($sign>0)):
	      	$time_stamp.='Zítra)';
	      elseif (($dist==1)&&($sign<0)):
	      	$time_stamp.='Včera)';
	      else:	
	      	$time_stamp.=($sign>0)?'za ':'před ';
	      	$time_stamp.=$dist;
	      	$time_stamp.=(($sign>0)&&($dist>4))?' dní)':' dny)';
	      endif;
	      //$time_stamp.='</font>';
	      return $time_stamp;	      
      }
      
      
      foreach ($aktuality as $aktualita) {


        if ($aktualita['type'] == 'ukoly')
          {
            if ($ukol = Ukoly::findOne($aktualita['id']))
            {
              echo '<p><strong>' . Html::a("<span class='glyphicon glyphicon-bell' aria-hidden='true'></span> $ukol->title") . 
                date_stamp($aktualita['date'],$aktualita['sign'],$aktualita['dist']) . ' </strong><br> ' 
                . $ukol->description . '</p>';
            }
          }
          
          elseif ($aktualita['type'] == 'document')
          {
            if ($doc = Document::findOne($aktualita['id']))
            {
              echo 
                '<p><strong>'.
                Html::a(
                  "<span class='glyphicon glyphicon-file' aria-hidden='true'></span> $doc->title"
                  , "files/" . $doc->file,['target' => '_blank']).
                date_stamp($aktualita['date'],$aktualita['sign'],$aktualita['dist']) ."</strong><br/>V sekci Dokumenty je nový dokument!</p>"; 
            }
          }
          
          elseif ($aktualita['type'] == 'news')
          {
            if ($news = News::findOne($aktualita['id']))
            {
              echo 
                '<p><strong>'.
                Html::a(
                  "<span class='glyphicon glyphicon-star' aria-hidden='true'></span> $news->title"
                  , $news->link).
                date_stamp($aktualita['date'],$aktualita['sign'],$aktualita['dist']) . "</strong><br/>$news->obsah</p>"; 
            }
          }
          elseif ($aktualita['type'] == 'prihlaseni')
          {
	        if ($event = Event::findOne($aktualita['id']))
            {
             echo '<p><strong><div style="display: inline;">';
              Modal::begin([
                'header' => "<h2>$event->title</h2>",
                'toggleButton' => [
                    'tag'   => 'a',
                    'label' => "<span class='glyphicon glyphicon-time' aria-hidden='true'></span> $event->title, termín přihlášení",
                    ],
              ]);

              echo $this->render('/event/_view',['model' => $event]);

              Modal::end();

              echo "</div><p style='display: inline;'>" . date_stamp($aktualita['date'],$aktualita['sign'],$aktualita['dist']) . "</p></strong>
              <br/>$event->description
              </p>";
            }
          }
          elseif ($aktualita['type'] == 'event')
          {
            if ($event = Event::findOne($aktualita['id']))
            {
              echo '<p><strong><div style="display: inline;">';
              Modal::begin([
                'header' => "<h2>$event->title</h2>",
                'toggleButton' => [
                    'tag'   => 'a',
                    'label' => "<span class='glyphicon glyphicon-tent' aria-hidden='true'></span> $event->title",
                    ],
              ]);

              echo $this->render('/event/_view',['model' => $event]);

              Modal::end();

              echo "</div><p style='display: inline;'>" . date_stamp($aktualita['date'],$aktualita['sign'],$aktualita['dist']) . "</p></strong>
              <br/>$event->description
              </p>";
            }
          }
      }
      ?>
      <br>

      <?php

      $funkce_all = $model->funkcePersonUnits;

      foreach ($funkce_all as $funkce) {
        //echo $funkce->funkce->name . ' - ' . $funkce->person->name . ' ' . $funkce->person->surname . '<br>';
      }

 ?>
      <a href="?r=site/kalendar"><img src="images/sipka_main_page.png" class="sipka"></a>
      </div>
      
      
      <br>
      <div class="titulni_box" id="box_orj">
      <p class="box_nadpis">Okresní rada</p>
      <div class="box_doplnkove_info">kontakt, lidé, poslání</div>
      <p>Junák - český skaut, okres Šumperk, z. s.<br>
      náměstí Jana Zajíce 11, 787 01 Šumperk<br>
      orjspk@skaut.cz</p>
      <a href="?r=site/orj"><img src="images/sipka_main_page.png" class="sipka"></a>
      </div>
      <br>
      <div class="titulni_box" id="box_khms">
      <p class="box_nadpis">Kritéria hodnocení a motivace středisek</p>
      <?= $this->render('/chart/_resultsOverallSmall', ['data'=>$resultChartData]); ?>
      <?= Html::a( Html::img("images/sipka_main_page.png", ['class'=>'sipka']) ,['chart/results-overall']) ?>
      </div>
</div>

<div class="col-md-5">
      <div class="titulni_box" id="box_onas">
      <p class="box_nadpis">Info o nás</p>
      <div class="box_doplnkove_info">kdo, kde, kontakty</div>
      <div class="mapa_main_page" style="margin-left:auto; margin-right: auto;  width: 80%">
   <?php 


GoogleMap::begin([
                        'id' => 'map-canvas',
                        'width' => '100%',
                        'height' => '180px'
                        ]) ?>
<script>
      function initialize() {
        var activeInfo;
        var heatmapData = [
        <?php 

        $members = Unit::GetORJSumperk()->getAllMembers(Registration::CURRENT_YEAR)->all();
        $latSum = 0;
        $count = 0;
        $lngSum = 0; 
        foreach ($members as $member)
        {
          if (!$member->lat or !$member->lng or $member->lat < 40 or $member->lng < 10) continue;
          $count++;
          $latSum += $member->lat;
          $lngSum += $member->lng;
        ?>
            new google.maps.LatLng(<?= $member->lat ?> ,<?= $member->lng ?>),
            <?
        }
        ?>
        ];
        var mapCenter = new google.maps.LatLng( <?= $latSum / ($count ? $count : 1) ?>, <?= $lngSum / ($count ? $count : 1) ?>);
        var mapOptions = {
          //center: { lat: <?= $latSum / ($count ? $count : 1) ?>, lng: <?= $lngSum / ($count ? $count : 1) ?>},
          center: { lat: 49.921765, lng: 16.878613},
          zoom: 11,
          //maxZoom: 13,
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);
        var heatmap = new google.maps.visualization.HeatmapLayer({
          data: heatmapData,
          //disipating: true.
          radius: 30,
        });
        heatmap.setMap(map);

        google.maps.event.addListener(map, 'zoom_changed', function() {
          var zoomLevel = map.getZoom();
          if (zoomLevel > 13)
          {
            heatmap.setMap(null);
          } else {
            heatmap.setMap(map);
          }
        });


        <?php
        $klubovny = Klubovna::find()->all();

        foreach($klubovny as $klubovna)
        {
            if (!$klubovna->lat or !$klubovna->lng or $klubovna->lat < 40 or $klubovna->lng < 10) continue;
            $units = $klubovna->GetAllUnits();
        ?>
            var marker<?= $klubovna->id ?> = new google.maps.Marker({
                position: new google.maps.LatLng(<?= $klubovna->lat ?> ,<?= $klubovna->lng ?>),
                map: map,
                title: '<?= $klubovna->name ?>',
                icon: '<?= Unit::ContainsCathegory($units, "Středisko") ? "images/stredisko.png" : "images/oddil.png" ?>',
            });

            var contentString = '<strong><?= $klubovna->name ?></strong>'
                              + '<ul>'
            <?php
            foreach ($units as $unit) {
              if ($unit->type == 'Družina') continue;
            ?>
                              + '<li><?= $unit->type ?> <?= Html::a($unit->name, ["chart/zakladna-vyvoj", "id"=>$unit->id]) ?></li>' 
            <?php
            }
            ?>
                              + '</ul>'

            var infoWindow<?= $klubovna->id ?> = new google.maps.InfoWindow({
               content: contentString,
            });

            google.maps.event.addListener(marker<?= $klubovna->id ?>, 'click', function() {
                if (activeInfo) activeInfo.close();
               infoWindow<?= $klubovna->id ?>.open(map,marker<?= $klubovna->id ?>);
               activeInfo = infoWindow<?= $klubovna->id ?>;
            });
        <?
        }
        
        ?>

      }
      google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php GoogleMap::end() ?>
    </div>
    <a href="?r=site/onas"><img src="images/sipka_main_page.png" class="sipka"></a>
      </div>
      
      
      <br>
      <div class="titulni_box" id="box_statistiky">
      <p class="box_nadpis">Statistiky</p>
      <div class="box_doplnkove_info">grafy, čísla, tabulky</div>
      <?= $this->render('/chart/_zakladna_pohlaviSmall', ['data'=>$model->zakladnaByPohlavi]); ?>
      <a href="?r=chart/zakladna-pohlavi&id=84"><img src="images/sipka_main_page.png" class="sipka"></a>
      </div>

</div>
