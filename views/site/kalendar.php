﻿<?
use yii\helpers\Html;
?>
<div class="col-md-1">
</div>
<div class="col-md-8">
<?php
	use yii\helpers\Url;

  	echo \yii2fullcalendar\yii2fullcalendar::widget(array(
      //'events'=> $events,
      'ajaxEvents' => Url::to(['jsoncalendar']),
      'options'=> [
      	'lang' => 'cs'
      ],
  	));
?>
</div>

<div class="col-md-3">
<h2>Pořádané akce</h2>
<p><? echo Html::a("Rádcovský kurz ŠRÁKY <span class='glyphicon glyphicon-new-window' aria-hidden='true'></span>", "http://sraky.wakan.cz",['target' => '_blank']); ?></p>
<p><? echo Html::a("Víkendový kurz ŠUMIK <span class='glyphicon glyphicon-new-window' aria-hidden='true'></span>", "http://orjspk.skauting.cz/sumik2016",['target' => '_blank']); ?></p>
<h2>Fotoarchiv</h2>
<? echo Html::a("FB fotoarchiv <span class='glyphicon glyphicon-new-window' aria-hidden='true'></span>", "https://www.facebook.com/orjspk/photos_stream?tab=photos_albums",['target' => '_blank']); ?>
</div>