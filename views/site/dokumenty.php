﻿<?php
/* @var $this yii\web\View */
$this->title = 'GISkaut';

use app\models\Unit;
use app\models\Registration;
use app\models\Person;
use app\models\Klubovna;
use app\widgets\GoogleCharts;
use app\widgets\GoogleMap;

use yii\grid\GridView;

use yii\helpers\Html;
use yii\widgets\ListView;

use app\models\FunkcePersonUnit;
use yii\data\ArrayDataProvider;
use app\models\Document;


?>
<h1>Dokumenty</h1>
<h2>Zápisy z rad</h2>

<!--

<?= GridView::widget([
        'dataProvider' => $zapisyDataProvider,
        'columns' => [
            'title',

            'created_at',
            [
                'value'=> function($data) {
                    return '<a href="files/'.$data->file.'" target="_blank"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>';
                },
                'format' => 'html'
            ],
        ],
    ]); ?>
//-->
<p><div id="w0" class="row">


      <?php
      $recent_year=0;
      foreach ($dokument as $d) {
	      if ($d->type=="zapis") {
		      if (Substr($d->file,strlen($d->file)-4,4)==".pdf"):
		      	$iconm="iconm_pdf.png";
		      else:
		      	$iconm="iconm_file.png";
		      endif;  	
		   	
		     
		      echo ($recent_year==substr($d->valid_from,0,4))?'':(($recent_year==0)?'':'</div></div>').'<div class="col-sm-6 col-md-4"><div class="box_zapisy"><div class="rok_dokumenty"><strong>'.substr($d->valid_from,0,4).'</strong></div>';
		      echo '<a href="files/'.$d->file.'" target="_blank"><img src="images/'.$iconm.'">&nbsp;'.$d->title.'</a><br>';
		      $recent_year=substr($d->valid_from,0,4);
	      }
      }

?>
</div></div>
</div>
</p>
<h2>Vyhlášky a řády</h2>
<p>
<?
      foreach ($dokument as $d) {
	      if ($d->type=="vyhlaska") {
		  	echo '<a href="files/'.$d->file.'" target="_blank"><img src="images/'.$iconm.'">&nbsp;'.$d->title.'</a><br>';
 
	      }
      }

?>
</p>
<h2>Výroční zprávy</h2>
<p>
<?
      foreach ($dokument as $d) {
	      if ($d->type=="vz") {
		  	echo '<a href="files/'.$d->file.'" target="_blank"><img src="images/'.$iconm.'">&nbsp;'.$d->title.'</a><br>';
 
	      }
      }

?>
</p>
<h2>Ostatní</h2>
<p>
<?
      foreach ($dokument as $d) {
	      if ($d->type=="jine") {
		  	echo '<a href="files/'.$d->file.'" target="_blank"><img src="images/'.$iconm.'">&nbsp;'.$d->title.'</a><br>';
 
	      }
      }

?>
</p>

