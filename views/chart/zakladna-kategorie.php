<?php $this->beginContent('@app/views/layouts/unitsSidebar.php'); ?>

<?php
	$this->params['model'] = $model;
	$this->params['action'] = 'chart/zakladna-kategorie';
	$this->title = $model->name;
?>

<?php
	use app\widgets\GoogleCharts;
	use app\models\Registration;
	use app\models\Unit;

	$graphs = [];

	$cathegories = Registration::GetCathegories();

	foreach (array_keys($cathegories) as $cathegory)
	{
		$graphs[] = 
		[
		//'balloonText' => '[[title]] in [[category]]:<b>[[value]]</b>',
		'fillAlphas' => 0.6,
		'title' => $cathegory,
		'valueField' => $cathegory,
		'balloonText' => '[[title]] :<b>[[value]]<b>',
		];
	}
	
	
	$chartConfiguration = [

	    'type'         => 'serial',
	    'dataProvider' => $data,
	    'theme'=> 'none',
	    'legend' 	   => [
	    	        //'horizontalGap' => 10,
			        //'maxColumns' => 1,
			        'equalWidth' => false,
			        //'periodValueText' => 'celkem: [[value.sum]]',
			        //'position' => 'bottom',
			        //'valueWidth' => 80,
	    ],

	   'categoryField' =>  'year',

	   //'chartScrollbar' => [],

	   'plotAreaBorderAlpha' => 0,
	   'chartCursor' => [
	   		'cursorAlpha' => 0,
	   ],
	 
	   'categoryAxis' => [ //'gridPosition' => 'start', 
	  						'axisColor' => '#DADADA',
	   						'startOnAxis' => true,

	   					],

	   'valueAxes'    => [['axisAlpha' => 1,
	   					   'stackType' => 'regular',
	   					   'gridAlpha' => 0.07,
	   					   'position'  => 'left',
	   					   'title'     => 'Počet členů',
	   					   ]],

	   'graphs'       => $graphs,
	];

	echo speixoto\amcharts\Widget::widget(['chartConfiguration' => $chartConfiguration, 'width'=>'100%']); 
?>


<?php $this->endContent(); ?>
