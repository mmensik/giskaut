
<?php
	use app\widgets\GoogleCharts;
	use app\models\KriteriaGroup;
	use app\models\Unit;

	use  yii\web\View;

	$graphs = [];
	$kategorie = KriteriaGroup::find()->all();

	foreach ($kategorie as $kat) 
	{
		$graphs[] = 
		[
		//'balloonText' => '[[title]] in [[category]]:<b>[[value]]</b>',
		'type' => 'column',
		'fillAlphas' => 0.6,
		'title' => $kat->name,
		'valueField' => $kat->id,
		'columnWidth' => 0.5,
		'balloonText' => '[[title]] :<b>[[value]]<b>',
		];
	}
	
	$chartConfiguration = [

	    'type'         => 'serial',
	    'dataProvider' => $data,
	    'theme'=> 'none',
	    /*'legend' 	   => [
	    	        //'horizontalGap' => 10,
			        //'maxColumns' => 1,
			        'equalWidth' => false,
			        //'periodValueText' => 'celkem: [[value.sum]]',
			        'position' => 'right',
			        //'valueWidth' => 80,
	    ],*/

	   'categoryField' =>  'unit_short',

	   //'chartScrollbar' => [],
	   'startDuration' => 1,

	   'plotAreaBorderAlpha' => 0,
	   'chartCursor' => [
	   		'cursorAlpha' => 0,
	   ],
	 
	   'categoryAxis' => [ 'gridPosition' => 'start', 
	   						'gridAlpha' => 0.00,
	  						'axisColor' => '#DADADA',
	   						//'startOnAxis' => true,
	   						'labelRotation' => 70,

	   					],
	   	'chartCursor' => [
	   						'adjustment'=>0,
	   						//'categoryBalloonFunction'=> 'ballonFun'

	   	],

	  	'valueAxes'    => [['axisAlpha' => 0,
	   					   'stackType' => 'regular',
	   					   'gridAlpha' => 0.07,
	   					   'position'  => 'right',
	   					   'labelsEnabled' => false
	   					   //'offset' => 15,
	   					   //'title'     => 'Počet členů',
	   					   ]],

	   'graphs'       => $graphs,
	];

	echo speixoto\amcharts\Widget::widget(['chartConfiguration' => $chartConfiguration, 'width'=>'100%', 'height'=>'300px']); 
?>


