<?php $this->beginContent('@app/views/layouts/unitsSidebar.php'); ?>

<?php
/* @var $this yii\web\View */
$this->params['model'] = $model;
$this->params['action'] = 'chart/unit_kriteria';
$this->params['extra_params'] = ['kategorie_id' => $kategorie->id];
$this->params['show_subunits'] = false;


?>

<h1>Vysledky v kategorii <?= $kategorie->name ?></h1>

      <?php   

      
      	$graphs[] = 
		[
		//'balloonText' => '[[title]] in [[category]]:<b>[[value]]</b>',
		'type' => 'column',
		'fillAlphas' => 0.6,
		//'title' => 'xxxx',
		'valueField' => 'body',
		'balloonText' => '[[category]] :<b>[[value]]<b>',
		'labelText' => '[[value]]',
		'labelPosition' => 'top',
		'fillColorsField' => 'color',
		'lineAlpha' => 1,
		'lineColor' => '#9E9E9E',
		];

      	$chartConfiguration = [

	    'type'         => 'serial',
	    'dataProvider' => $dataKriterium,
	    'theme'=> 'none',
	    /*'legend' 	   => [
	    	        //'horizontalGap' => 10,
			        //'maxColumns' => 1,
			        'equalWidth' => false,
			        //'periodValueText' => 'celkem: [[value.sum]]',
			        'position' => 'right',
			        //'valueWidth' => 80,
	    ],*/

	   'categoryField' =>  'short_name',

	   //'chartScrollbar' => [],
	   'startDuration' => 1,

	   'plotAreaBorderAlpha' => 0,
	   'columnWidth' => 0.65,
	   'chartCursor' => [
	   		'cursorAlpha' => 0,
	   		'categoryBalloonEnabled' => false,
	   ],
	 
	   'categoryAxis' => [ //'gridPosition' => 'start', 
	  						'axisColor' => '#DADADA',
	   						//'startOnAxis' => true,
	   						'labelRotation' => 65,

	   					],

	   'valueAxes'    => [['axisAlpha' => 1,
	   					   'stackType' => 'regular',
	   					   'gridAlpha' => 0.07,
	   					   'position'  => 'left',
	   					   'offset' => 45,
	   					   'guides' => [
	   							[
	   								'lineColor' => '#FDB427',
	   								'lineAlpha' => 1,
	   								'lineThickness' => 1,
	   								'dashLength' => 5,
	   								'value' => $line_value
	   							]
	   						]
	   					   //'title'     => 'Počet členů',
	   					   ]],

	   'graphs'       => $graphs,
	];

	echo speixoto\amcharts\Widget::widget(['chartConfiguration' => $chartConfiguration, 'width'=>'100%', 'height'=>'380px']);

?>

<?php $this->endContent(); ?>