<?php $this->beginContent('@app/views/layouts/unitsSidebar.php'); ?>

<?php
	$this->params['model'] = $model;
	$this->params['action'] = 'chart/zakladna-vek';
	$this->title = $model->name;
?>

<?php
	use app\widgets\GoogleCharts;
	use app\models\Registration;
	use app\models\Unit;

	$graphs = [[
		'title' => 'Počet členů',
		'valueField' => 'headCount',
		'type'=>'column',
		'lineAlpha' => 1,
		//'lineThickness' => 3,
		//'bullet' => 'round',
		//'bulletSize'	=> 7,
		'bulletBorderAlpha' => 1,
		//'lineColor' => '#444444',
		'fillAlphas' => 1,
		'balloonText' => '[[title]] ve věku [[category]]:<b>[[value]]</b>'
	]];
	
	$chartConfiguration = [

	    'type'         => 'serial',
	    'dataProvider' => $data,
	    'theme'=> 'none',
	    'legend' 	   => [
	    	        //'horizontalGap' => 10,
			        //'maxColumns' => 1,
			        'equalWidth' => false,
			        //'periodValueText' => 'celkem: [[value.sum]]',
			        //'position' => 'bottom',
			        //'valueWidth' => 80,
	    ],

	   'categoryField' =>  'age',

	   //'chartScrollbar' => [],

	   'plotAreaBorderAlpha' => 0,
	   'chartCursor' => [
	   		'cursorAlpha' => 0,
	   ],
	 
	   'categoryAxis' => [  'gridPosition' => 'start', 
	  						'axisColor' => '#DADADA',
	   						'startOnAxis' => true,
	   						'equalSpacing' => true,
	   						//'cathegoryFunction' => '(int)'

	   					],

	   'valueAxes'    => [['axisAlpha' => 1,
	   					   'stackType' => 'regular',
	   					   'gridAlpha' => 0.07,
	   					   'position'  => 'left',
	   					   'title'     => 'Počet členů',
	   					   ]],

	   'graphs' => $graphs,
	];

	echo speixoto\amcharts\Widget::widget(['chartConfiguration' => $chartConfiguration, 'width'=>'100%']); 
?>

<?php $this->endContent(); ?>