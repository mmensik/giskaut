﻿
<?php
	$this->title = 'Celkove vysledky';

	use yii\helpers\Html;
	


?>
<!--<h1>Kritéria hodnocení a motivace středisek</h1>//-->

<div style="border: solid 0px black; position: relative">

	<script>
	<!--

		function navigatory(XPullDown) {
		location.href=(document.XPullDown.YList.options[document.XPullDown.YList.selectedIndex].value);
		}
		
	// -->
	</script>
	
	<div style="border: solid 0px black; position: absolute; top: 2px; left: 350px">	
		<form name="XPullDown">
		Zvol jiný rok: 
		<select name="YList" onChange="navigatory(this.form)" size="1">
		<?
		foreach ($seasons as $rocnik) {
			echo "<option value=\"?r=chart/results-overall&seasonId=".$rocnik->id."\"".(($rocnik->id==$season->id)?" SELECTED":"").">".$rocnik->name."</option>";		
		}
		?>
		</select>
		</form>
	</div>
	<h2>Stav KHMS, rok <? echo $season-> name; ?></h2>	
</div>			
			
<?php

	use app\widgets\GoogleCharts;
	use app\models\KriteriaGroup;
	use app\models\Unit;

	$graphs = [];
	$kategorie = KriteriaGroup::find()->all();

	foreach ($kategorie as $kat) 
	{
		$graphs[] = 
		[
		//'balloonText' => '[[title]] in [[category]]:<b>[[value]]</b>',
		'type' => 'column',
		'fillAlphas' => 0.4,
		'title' => $kat->name,
		'valueField' => $kat->id,
		'balloonText' => '[[title]] :<b>[[value]]<b>',
		];
	}
	
	$chartConfiguration = [

	    'type'         => 'serial',
	    'dataProvider' => $data,
	    'theme'=> 'none',
	    'legend' 	   => [
	    	        'horizontalGap' => 10,
			        //'maxColumns' => 1,
			        'equalWidth' => false,
			        //'periodValueText' => 'celkem: [[value.sum]]',
			        //'position' => 'right',
			        //'valueWidth' => 80,
			        'borderAlpha' => 0.2
	    ],
	   'creditsPosition' => 'top-right',
	   'categoryField' =>  'unit_short',
	   'columnWidth' => 0.5,

	   //'chartScrollbar' => [],
	   'startDuration' => 1,

	   'plotAreaBorderAlpha' => 0.2,
	   'chartCursor' => [
	   		'cursorAlpha' => 0,
	   ],
	 
	   'categoryAxis' => [ 'gridPosition' => 'start', 
	  						'axisColor' => '#DADADA',
	   						//'startOnAxis' => true,
	   						'labelRotation' => 0
	   					],

	   'valueAxes'    => [['axisAlpha' => 1,
	   					   'stackType' => 'regular',
	   					   'gridAlpha' => 0.1,
	   					   'position'  => 'left',
	   					   //'offset' => 45,
	   					   'totalText' => '[[total]]',
	   					   'title'     => 'Body',
	   					   ]],

	   'graphs'       => $graphs,
	];

	
	$strediska = Unit::find()->where(['and',['type' => 'Středisko'],['super_unit_id' => '84']])->all();
	
	?>
	<div class="col-md-9">
	<?
	
	echo speixoto\amcharts\Widget::widget(['chartConfiguration' => $chartConfiguration, 'width'=>'100%', 'height'=>'530px']);

	?>
	</div>
	<div class="col-md-3">
	<h2>Více o střediscích</h2>
	<br>
	<?
	
	foreach ($strediska as $stredisko) {
		echo "<p>" . Html::a($stredisko->name, ['site/khmsstredisko', 'aktualni_stredisko'=>$stredisko->id, 'season'=>$season->id]) . "</p>";
	}

	?>
	</div>
	<?	
		
	echo $this->render('/season/_tableResults', ['results' => $results, 'kriterias' => $kriterias, 'units'=>$units]);
?>
