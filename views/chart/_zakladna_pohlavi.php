<?php
	use app\widgets\GoogleCharts;
	use app\models\Registration;
	use app\models\Unit;

	$graphs = [[
		//'balloonText' => '[[title]] in [[category]]:<b>[[value]]</b>',
		'fillAlphas' => 0.4,
		'title' => 'Kluci',
		'valueField' => 'male',
		'balloonText' => '<b>[[value]]<b>',
	],
	[
		//'balloonText' => '[[title]] in [[category]]:<b>[[value]]</b>',
		'fillAlphas' => 0.4,
		'title' => 'Holky',
		'valueField' => 'female',
		'balloonText' => '<b>[[value]]</b>'
	]];
	
	$chartConfiguration = [

	    'type'         => 'serial',
	    'dataProvider' => $data,
	    'theme'=> 'none',
	    'legend' 	   => [
	    	        //'horizontalGap' => 10,
			        //'maxColumns' => 1,
			        'equalWidth' => false,
			        //'periodValueText' => 'celkem: [[value.sum]]',
			        //'position' => 'bottom',
			        'valueWidth' => 80,
	    ],

	   'categoryField' =>  'year',

	   //'chartScrollbar' => [],

	   'plotAreaBorderAlpha' => 0,
	   'chartCursor' => [
	   		'cursorAlpha' => 0,
	   ],
	 
	   'categoryAxis' => [ //'gridPosition' => 'start', 
	  						'axisColor' => '#DADADA',
	   						'startOnAxis' => true,
	   						//'labelsEnabled' => false,
	   					],

	   'valueAxes'    => [['axisAlpha' => 0,
	   					   'stackType' => 'regular',
	   					   'gridAlpha' => 0.07,
	   					   'position'  => 'left',
	   					   'labelsEnabled' => false,
	   					   //'title'     => 'Počet členů',
	   					   'labelsEnabled' => false,
	   					   ]],

	   'graphs'       => $graphs,
	];

	echo speixoto\amcharts\Widget::widget(['chartConfiguration' => $chartConfiguration, 'width'=>'100%']); 
?>
