<?php $this->beginContent('@app/views/layouts/unitsSidebar.php'); ?>

<?php
	$this->params['model'] = $model;
	$this->params['action'] = 'chart/zakladna-vyvoj';
	$this->title = $model->name;
?>

<?php
	use app\widgets\GoogleCharts;
	use app\models\Registration;
	use app\models\Unit;

	
	

	$units = $model->units;

	$graphs = [[
		'title' => $model->name,
		'valueField' => $model->id,
		'lineAlpha' => 1,
		'lineThickness' => 3,
		'bullet' => 'round',
		'bulletSize'	=> 7,
		'bulletBorderAlpha' => 1,
		'lineColor' => '#444444',
		'fillAlphas' => 0,
		'balloonText' => '[[title]] in [[category]]:<b>[[value]]</b>'
	]];
	foreach ($units as $unit)
	{
		$graphs[] = [
			'type' => 'column',
			'title' => $unit->name,
			'valueField' => $unit->id,
			'lineAlpha' => 0,
			'color' => '#000000',
			'fillAlphas' => 0.6,
			'balloonText' => '[[title]] v roce [[category]]:<b>[[value]]</b>'
		];
	}

	$chartConfiguration = [
	    'type'         => 'serial',
	    'dataProvider' => $data,
	    //'theme'=> 'chalk',
	    'legend' 	   => [
	    	        'horizontalGap' => 10,
			        //'maxColumns' => 1,
			        'position' => 'bottom',
					'useGraphSettings' => true,
					'markerSize' => 10
	    ],
	   'categoryField' =>  'year',
	 
	   'categoryAxis' => ['gridPosition' => 'start', 
	  						// 'axisColor' => '#DADADA'
	   					],
	   'valueAxes'    => [['axisAlpha' => 1,
	   					   'stackType' => 'regular',
	   					   'gridAlpha' => 0,
	   					   ]],
	   'graphs'       => $graphs,
	];
	echo speixoto\amcharts\Widget::widget(['chartConfiguration' => $chartConfiguration, 'width'=>'100%']); 
?>


<?php $this->endContent(); ?>
