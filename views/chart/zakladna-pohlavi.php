<?php 
	$this->beginContent('@app/views/layouts/unitsSidebar.php');

	$this->params['model'] = $model;
	$this->params['action'] = 'chart/zakladna-pohlavi';
	$this->title = $model->name;

	echo $this->render('_zakladna_pohlavi.php', ['data' => $data]);

	$this->endContent(); 
?>
