<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\Person;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'People';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Person', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            'surname',
            'nickname',
            [
                'value' => function($data) {
                    $unit = $data->lastUnit;

                    return $unit ? $unit->name : "";
                }
            ],
            // 'birthday',
            // 'sex',
            // 'city',
            // 'street',
            // 'country',
            // 'zip',
            // 'cathegory',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
