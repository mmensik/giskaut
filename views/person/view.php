<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'People', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'surname',
            'nickname',
            'birthday',
            'sex',
            'city',
            'street',
            'country',
            'zip',
            'cathegory',
        ],
    ]) ?>

</div>

<?php

use app\widgets\GoogleMap;

?>

<?php GoogleMap::begin([
                        'id' => 'map-canvas',
                        'width' => '600px',
                        'height' => '600px'
                        ]) ?>
<script>
      var geocoder;
  var map;
  function initialize() {
    geocoder = new google.maps.Geocoder();

    var latlng = new google.maps.LatLng(-34.397, 150.644);
    var mapOptions = {
      zoom: 12,
      center: latlng
    }
    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    codeAddress();
  }

   google.maps.event.addDomListener(window, 'load', initialize);

  function codeAddress() {
    var address = '<?= $model->street . " "  . $model->city . "," . $model->zip . " ," . $model->country ?>';
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });

  }
</script>
<?php GoogleMap::end() ?>
