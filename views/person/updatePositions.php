<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

$form = ActiveForm::begin();
?>
<div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
</div>
<?php
$counter = 0;
foreach ($persons as $index => $person) {
  
  if (($person->lat and $person->lng) or !$person->city ) continue;

  $counter++;
  if ($counter > 150) break;

	echo "<h3>$person->name $person->surname</h3>";
    echo $form->field($person, "[$index]lat");
    echo $form->field($person, "[$index]lng");

	$this->registerJs("findAddress('$person->city $person->street, $person->zip, $person->country', '#person-$index-lat', '#person-$index-lng');", 
		View::POS_LOAD);


}

ActiveForm::end();


$js =<<<EOT
  var geocoder;
  var map;
  function initialize() {
    geocoder = new google.maps.Geocoder();
  }

  function findAddress(a1, id_lat, id_lng) {

  	geocoder.geocode( { 'address': a1 }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      	$(id_lat).val(results[0].geometry.location.lat());
      	$(id_lng).val(results[0].geometry.location.lng());
      } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
      	setTimeout(function() { findAddress(a1, id_lat, id_lng); } , 500);
      }
    });
  }
EOT;

	$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyC-0ySrdapWeOB2JwywCH_ZXjMwmHEFBxs', ['position' => View::POS_HEAD]);
	$this->registerJs("initialize();", View::POS_READY);
	$this->registerJs($js, View::POS_HEAD);
	//$this->registerJs("initialize();", View::POS_LOAD);
?>