<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="person-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rc')->textInput(['maxlength' => 12]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 32]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => 32]) ?>

    <?= $form->field($model, 'nickname')->textInput(['maxlength' => 32]) ?>

    <?= $form->field($model, 'birthday')->textInput() ?>

    <?= $form->field($model, 'telephone')->textInput() ?>

    <?= $form->field($model, 'sex')->textInput(['maxlength' => 11]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => 32]) ?>

    <?= $form->field($model, 'street')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'zip')->textInput() ?>

    <?= $form->field($model, 'cathegory')->textInput(['maxlength' => 16]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
