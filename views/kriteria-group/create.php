<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KriteriaGroup */

$this->title = 'Create Kriteria Group';
$this->params['breadcrumbs'][] = ['label' => 'Kriteria Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kriteria-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
