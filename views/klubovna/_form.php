<?php

use app\models\Person;

use yii\web\View;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Button;
use app\widgets\GoogleMap;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Klubovna */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="klubovna-form">

    <?php $form = ActiveForm::begin(); ?>


    

<div class="row">
    <div class="col-md-5">

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'spravce_id')->dropDownList( ['' => ''] + Person::GetDropdownItems()) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zip')->textInput() ?>

    <script type="text/javascript">
    function initButtons() {
        $("#getLatLngBtn").on("click", function(event) {
            codeAddress($("#klubovna-street").val() + "," + $("#klubovna-city").val() + "," + $("#klubovna-zip").val() + "," + $("#klubovna-country").val());
            //codeAddress();
            event.preventDefault();
        });
    }
    </script>
    <?php $this->registerJs("initButtons()", View::POS_READY);?>
    </div>

    <div class="col-md-7">

            <?php
                $inputSet = ['template' => '<div class="col-md-4">{label}{input}</div>', 'options'=>['tag'=>'span']];
            ?>

            <div class="row" style="padding-bottom: 15px">

            <?= $form->field($model, 'lat', $inputSet)->textInput() ?>

            <?= $form->field($model, 'lng',$inputSet)->textInput() ?>

            <div class="col-md-4" style="padding-top: 25px">
             <?= Button::widget(['label'=>'Aktualizuj polohu', 'id'=>'getLatLngBtn']) ?>
             </div>
            </div>

<?php 

    GoogleMap::begin([
                            'id' => 'map-canvas',
                            'width' => '100%',
                            'height' => '400px'
                            ])
    ?>
    <script>
      var geocoder;
      var map;
      var marker;
      function initialize() {
        geocoder = new google.maps.Geocoder();

        var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {
          zoom: 12,
          center: latlng
        }
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        var pos = new google.maps.LatLng(<?= $model->lat ? $model->lat : 0?>,<?= $model->lng ? $model->lng : 0 ?>);
        marker = new google.maps.Marker({
                map: map,
                position: pos,
                draggable: true,
                title : "Sídlo klubovny",
        });

        google.maps.event.addListener(marker, 'dragend', function() {
            $("#klubovna-lat").val(marker.getPosition().lat());
            $("#klubovna-lng").val(marker.getPosition().lng());
        });

        map.setCenter(pos);
        //codeAddress('<?= $model->street . " "  . $model->city . "," . $model->zip . " ," . $model->country ?>');
      }

       google.maps.event.addDomListener(window, 'load', initialize);

      function codeAddress(address) {
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            marker.setPosition(results[0].geometry.location);
            $("#klubovna-lat").val(results[0].geometry.location.lat());
            $("#klubovna-lng").val(results[0].geometry.location.lng());
          } else {
            //alert("Geocode was not successful for the following reason: " + status);
          }
        });

      }
    </script>
<?php GoogleMap::end() 

?>
    </div>
</div>

<?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>




    <?php ActiveForm::end(); ?>

</div>
