<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Klubovnas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="klubovna-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Klubovna', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'description:ntext',
            'city',
            'street',
            // 'country',
            // 'zip',
            // 'lat',
            // 'lng',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
