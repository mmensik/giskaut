<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Klubovna */

$this->title = 'Create Klubovna';
$this->params['breadcrumbs'][] = ['label' => 'Klubovnas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="klubovna-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
