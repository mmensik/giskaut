<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MailSent */

$this->title = 'Update Mail Sent: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mail Sents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mail-sent-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
