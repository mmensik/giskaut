<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MailSent */

$this->title = 'Create Mail Sent';
$this->params['breadcrumbs'][] = ['label' => 'Mail Sents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-sent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
