<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MailSent */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mail Sents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-sent-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'reason',
            'reason_id',
            'subject',
            'adresses:ntext',
            'body:ntext',
            'created_at',
        ],
    ]) ?>

</div>
