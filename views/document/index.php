<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\Document;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Documents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Document', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'title',
            [
                'value'=> function($data) {
                    return Document::GetTypes()[$data->type];
                },
                'label' => 'Typ'
            ],
            'created_at',
            'valid_from',
            //'file',
            // 'user_created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
