<?php

use yii\helpers\Html;
use app\models\Document;

use kartik\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Document */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="document-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(Document::GetTypes()) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?php
        if ($model->isNewRecord)
        {
            echo $form->field($model, 'uploadedFile')->fileInput();
        }
     ?>

    <?= $form->field($model, 'valid_from')->widget(DateTimePicker::classname(), [
        'pluginOptions' => [
            'autoclose' => true,
            //'format' => 'yyyy-mm-dd'
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
