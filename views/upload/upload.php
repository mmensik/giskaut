<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'year') ?>

<?= $form->field($model, 'file')->fileInput() ?>

<div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Odeslat', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>



<?php ActiveForm::end() ?>

<?php

	$provider = new ArrayDataProvider([
	    'allModels' => $status,
	    'pagination' => [
	    	'pageSize' => 200,
	    ],
	]);

?>

<?= GridView::widget([
    'dataProvider' => $provider,
]) ?>
