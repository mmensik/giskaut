<?php

use yii\helpers\Html;

use yii\web\View;

use yii\bootstrap\Tabs;

use app\models\Registration;
use app\widgets\GoogleCharts;

/* @var $this yii\web\View */
/* @var $model app\models\Unit */

$this->title = $model->name;
/*
$this->params['breadcrumbs'][] = ['label' => 'Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
*/

?>
<div class="unit-view">

    <h1><?= Html::encode($this->title) ?></h1>

<?php
    echo $this->render('_view', ['model'=>$model]);
?>
</div>