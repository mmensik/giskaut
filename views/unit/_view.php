    <?php
use yii\widgets\DetailView;
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            'name',
            [
                'label'=>'Klubovna',
                'value'=> $model->klubovna ? $model->klubovna->name : "Není zadána",
            ],
            'super_unit_id',
            'type',
        ],
    ]) ?>
    