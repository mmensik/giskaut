<?php

use yii\helpers\Html;
use yii\bootstrap\Button;
use yii\data\ActiveDataProvider;

use app\models\FunkcePersonUnit;
use yii\grid\GridView;

use kartik\sortable\Sortable;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Unit */

$this->title = 'Update Unit: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="unit-update">
<h1><?= Html::encode($this->title) ?></h1>
<div class="col-md-7">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
 </div>

 <div class="col-md-5">
 <h2>Funkce</h2>

<div id="funkce_table">
<?php

    $data = FunkcePersonUnit::find()->where(['id_unit' => $model->id])->orderBy('order')->all();

    $items = [];
    foreach ($data as $row) {
        $options = [
                    'title' => Yii::t('yii', 'Delete'),
                    'aria-label' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0'
        ];
        $items[] = ['content' => 
            Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['delfunkce', 'id'=>$model->id, 'funkce_id'=>$row->id_funkce, 'person_id'=>$row->id_person]), $options) 
            . ' <strong>' . $row->funkce->name . '</strong> - ' . $row->person->surname . ' ' . $row->person->name
        , 'options'=>['id_funkce' => $row->id_funkce, 'id_person' => $row->id_person, 'id_unit' => $row->id_unit]];
    }

    echo Sortable::widget([
        'items' => $items, 
        'pluginEvents' => [
             'sortupdate' => "function() { sortUpdate('". Url::to(['sort'])  ."'); }",
        ],
        ]
        );

$js =<<<EOT
function sortUpdate(action)
{
    var data = [];
    $('#funkce_table li').each(function() {
        data.push(
            {
            'key' : {
            'id_funkce' : $(this).attr('id_funkce'),
            'id_person' : $(this).attr('id_person'), 
            'id_unit' : $(this).attr('id_unit')
            },
            'order' : $(this).index()
            }
        
        );
    });

     $.ajax({
                    'url': action,
                    'type': 'post',
                    'data': {'items': JSON.stringify(data)},
                    'success': function () {
                       
                    },
                    'error': function (request, status, error) {
                        alert(status + ' ' + error);
                    }
                });
};
EOT;

    $this->registerJs("", View::POS_READY);
    $this->registerJs($js, View::POS_HEAD);

?>
</div>
<div>
	<?= Html::a('Pridat', ['addfunkce', 'id'=>$model->id], ['class'=>'btn btn-primary']); ?>
</div>


 </div>


</div>
