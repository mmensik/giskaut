<?php
	use app\widgets\GoogleCharts;
	use app\models\Registration;
	use app\models\Unit;

	$years = Registration::GetYears();

	$units = $model->units;

	$headCounts[0] = ['Rok', $model->name];
	foreach ($units as $unit)
	{
		$headCounts[0][] = $unit->name;
	}

	foreach ($years as $year)
	{
		$hc = ["$year", (int)$model->getAllMembers($year)->count()];

		foreach ($units as $unit)
		{
			$hc[] = (int)$unit->getAllMembers($year)->count();
		}

		$headCounts[] = $hc;
	}
?>

<?= GoogleCharts::widget([
'id' => 'my-id',
'visualization' => 'ComboChart',
'dataArray' => $headCounts,
'options' => [
    'title' => 'Vývoj počtu členů',
    'width' => 600,
    'height' => 600,
    'is3D' => true,
    'seriesType' => 'bars',
    'series' => [0 => ['type' => 'line', 'targetAxisIndex' => 1]]

],
'responsive' => true,]) ?>