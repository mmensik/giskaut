<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Person;
use app\models\Funkce;


/* @var $this yii\web\View */
/* @var $model app\models\Funkce */

$this->title = 'Prirazeni funkce do ' . $unit->name;
?>

<div class="funkce-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    

    <?= $form->field($model, 'id_person')->dropDownList( ['' => ''] + Person::GetDropdownItems()) ?>

    <?= $form->field($model, 'id_funkce')->dropDownList( ['' => ''] + Funkce::GetDropdownItems()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Pridat' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>