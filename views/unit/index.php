<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\Registration;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jednotky';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?> 

    <p>
        <?= Html::a('Založit jednotku', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            
            'code',
            'name',
            [
                'value'=> function($data) {
                    $su = $data->superUnit;
                    return $su ? $su->name : "";
                },
                'label' => 'Nadřízená jednotka'
            ],
            [
                'value'=> function($data) {
                    return $data->getAllMembers(Registration::CURRENT_YEAR)->count();
                },
                'label' => 'Pocet clenu'
            ],
            'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
