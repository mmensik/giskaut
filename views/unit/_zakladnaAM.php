<?php
	use app\widgets\GoogleCharts;
	use app\models\Registration;
	use app\models\Unit;

	$years = Registration::GetYears();

	$units = $model->units;

	$graphs = [[
		'title' => $model->name,
		'valueField' => $model->id,
		'lineAlpha' => 1,
		'lineThickness' => 3,
		'bullet' => 'round',
		'bulletSize'	=> 7,
		'bulletBorderAlpha' => 1,
		'lineColor' => '#444444',
		'fillAlphas' => 0,
		'balloonText' => '[[title]] in [[category]]:<b>[[value]]</b>'
	]];
	foreach ($units as $unit)
	{
		$graphs[] = [
			'type' => 'column',
			'title' => $unit->name,
			'valueField' => $unit->id,
			'lineAlpha' => 0,
			'color' => '#000000',
			'fillAlphas' => 0.6,
			'balloonText' => '[[title]] in [[category]]:<b>[[value]]</b>'
		];
	}

	$headCounts = [];
	foreach ($years as $year)
	{
		$hc['year'] = $year; 
		$hc[$model->id] = (int)$model->getAllMembers($year)->count();

		foreach ($units as $unit)
		{
			$hc[$unit->id] = (int)$unit->getAllMembers($year)->count();
		}

		$headCounts[] = $hc;
	}

	$chartConfiguration = [
	    'type'         => 'serial',
	    'dataProvider' => $headCounts,
	    //'theme'=> 'chalk',
	    'legend' 	   => [
	    	        'horizontalGap' => 10,
			        'maxColumns' => 1,
			        'position' => 'right',
					'useGraphSettings' => true,
					'markerSize' => 10
	    ],
	   'categoryField' =>  'year',
	 
	   'categoryAxis' => ['gridPosition' => 'start', 
	  						// 'axisColor' => '#DADADA'
	   					],
	   'valueAxes'    => [['axisAlpha' => 1,
	   					   'stackType' => 'regular',
	   					   'gridAlpha' => 0,
	   					   ]],
	   'graphs'       => $graphs,
	];
	echo speixoto\amcharts\Widget::widget(['chartConfiguration' => $chartConfiguration, 'width'=>'100%']); 
?>

