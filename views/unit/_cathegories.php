<?php
	use app\widgets\GoogleCharts;
	use app\models\Registration;
	use app\models\Unit;

	$years = Registration::GetYears();
	$cathegories = Registration::GetCathegories();

	$data[0] = ['Rok'];
	foreach ($cathegories as $name => $cathegory)
	{
		$data[0][] = $name;
	}

	foreach ($years as $year)
	{
		$row = ["$year"];

		foreach ($cathegories as $name => $cathegory)
		{
			$row[] = (int)$model->getAllMembers($year, $cathegory)->count();
		}

		$data[] = $row;
	}
?>

<?= GoogleCharts::widget([
'id' => 'cathegories_chart',
'visualization' => 'ComboChart',
'dataArray' => $data,
'options' => [
    'title' => 'Vývoj počtu členů',
    'width' => 600,
    'height' => 600,
    'is3D' => true,
    'seriesType' => 'bars',
    //'series' => [0 => ['type' => 'line']]

],
'responsive' => true,]) ?>