<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\Unit;
use app\models\Klubovna;

/* @var $this yii\web\View */
/* @var $model app\models\Unit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'web')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'super_unit_id')->dropDownList( ['' => ''] + ArrayHelper::map(Unit::find()
    																			->where(['type' => Unit::GetSuperCathegory()[$model->type]])
    																			->all()
    																			, 'id', 'name')) ?>

    <?= $form->field($model, 'klubovna_id')->dropDownList( ['' => ''] + ArrayHelper::map(Klubovna::find()->all(), 'id', 'name')) ?>


    <?= $form->field($model, 'type')->dropDownList(Unit::GetTypesArray(), ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
