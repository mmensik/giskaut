<?php
	use app\widgets\GoogleCharts;
	use app\models\Registration;
	use app\models\Unit;
	
	$year = Registration::CURRENT_YEAR;

	$result = $model->getAllMembers($year)
					->select(["$year - YEAR(birthday) as 'age'"])
					->orderBy('age')
					->asArray()->all();
				

	$data = [];
	for ($i = 0; $i < 100; $i++) $data[$i] = 0;
	foreach ($result as $row)
	{
		$data[$row['age']]++;
	}

	$filteredData[0] = ['Věk', 'Počet členů'];
	foreach ($data as $key => $row)
	{
		if ($row > 0) $filteredData[] = [$key, $row];
	}
	
?>

<?= 

GoogleCharts::widget([
'id' => 'age_chart',
'visualization' => 'ColumnChart',
'dataArray' => $filteredData,
'options' => [
    'title' => 'Věková struktura',
    'width' => 600,
    'height' => 600,
    'is3D' => true,
    //'series' => [0 => ['type' => 'line']
	],
'responsive' => true,])

 ?>