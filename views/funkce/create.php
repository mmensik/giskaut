<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Funkce */

$this->title = 'Create Funkce';
$this->params['breadcrumbs'][] = ['label' => 'Funkces', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funkce-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
