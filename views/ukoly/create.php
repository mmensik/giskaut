<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ukoly */

$this->title = 'Create Ukoly';
$this->params['breadcrumbs'][] = ['label' => 'Ukolies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ukoly-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
