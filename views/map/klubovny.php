<?php $this->beginContent('@app/views/layouts/unitsSidebar.php'); ?>

<?php
	$this->params['model'] = $model;
	$this->params['action'] = 'map/klubovny';
	$this->title = $model->name;
?>

<?php 
use app\widgets\GoogleMap;
use app\models\Registration;



GoogleMap::begin([
                        'id' => 'map-canvas',
                        'width' => '100%',
                        'height' => '600px'
                ]) ?>
<script>
      function initialize() {
        var mapOptions = {
          zoom: 11,
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);

        <?php 
        $latSum = 0;
        $count = 0;
        $lngSum = 0; 
        foreach ($klubovny as $member)
        {
          if (!$member->lat or !$member->lng or $member->lat < 40 or $member->lng < 10) continue;
          $count++;
          $latSum += $member->lat;
          $lngSum += $member->lng;
        ?>
        var marker<?= $member->id ?> = new google.maps.Marker({
        	position: new google.maps.LatLng(<?= $member->lat ?> ,<?= $member->lng ?>),
        	map: map,
        	title: '<?= $member->name ?>',
        });

        var infoWindow<?= $member->id ?> = new google.maps.InfoWindow({
	       content: '<?= $member->name ?>',
	      });

	    google.maps.event.addListener(marker<?= $member->id ?>, 'click', function() {
		   infoWindow<?= $member->id ?>.open(map,marker<?= $member->id ?>);
		});

        <?
        }
        ?>
      	var mapCenter = new google.maps.LatLng( <?= $latSum / ($count ? $count : 1) ?>, <?= $lngSum / ($count ? $count : 1) ?>);
      	map.setCenter(mapCenter);
      }
      google.maps.event.addDomListener(window, 'load', initialize);
</script>

<?php GoogleMap::end() ?>

<?php $this->endContent(); ?>