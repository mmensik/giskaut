<?php $this->beginContent('@app/views/layouts/unitsSidebar.php'); ?>

<?php
	$this->params['model'] = $model;
	$this->params['action'] = 'map/heat';
	$this->title = $model->name;
?>

<?php 
use app\widgets\GoogleMap;
use app\models\Registration;



GoogleMap::begin([
                        'id' => 'map-canvas',
                        'width' => '100%',
                        'height' => '600px'
                        ]) ?>
<script>
      function initialize() {
        var heatmapData = [
        <?php 

        $members = $model->getAllMembers(Registration::CURRENT_YEAR)->all();
        $latSum = 0;
        $count = 0;
        $lngSum = 0; 
        foreach ($members as $member)
        {
          if (!$member->lat or !$member->lng or $member->lat < 40 or $member->lng < 10) continue;
          $count++;
          $latSum += $member->lat;
          $lngSum += $member->lng;
        ?>
            new google.maps.LatLng(<?= $member->lat ?> ,<?= $member->lng ?>),
            <?
        }
        ?>
        ];
        var mapCenter = new google.maps.LatLng( <?= $latSum / ($count ? $count : 1) ?>, <?= $lngSum / ($count ? $count : 1) ?>);
        var mapOptions = {
          center: { lat: <?= $latSum / ($count ? $count : 1) ?>, lng: <?= $lngSum / ($count ? $count : 1) ?>},
          zoom: 11,
          maxZoom: 13,
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);
        var heatmap = new google.maps.visualization.HeatmapLayer({
          data: heatmapData,
          //disipating: true,
          //maxIntensity: 1,
          radius: 30,
        });
        heatmap.setMap(map);

        $('li a').on('shown.bs.tab', function(e)
        {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(mapCenter);
        });

      }
      google.maps.event.addDomListener(window, 'load', initialize);
</script>

<?php GoogleMap::end() ?>

<?php $this->endContent(); ?>