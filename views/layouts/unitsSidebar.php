<?php
	use app\models\Unit;
	use  yii\helpers\Html;
	use yii\helpers\ArrayHelper;
	use  yii\bootstrap\Nav;
	use yii\web\View;

	$model = $this->params['model'];
	$action = $this->params['action'];
	$extra_params = ArrayHelper::getValue($this->params, 'extra_params', []);
	$showSubunits = ArrayHelper::getValue($this->params, 'show_subunits', true);

	$activeStrediskoId = $model->id;

	if ($model->type != 'Okres')
	{
		$activeStrediskoId = $model->type == 'Středisko' ? $model->id : $model->superUnit->id;
	}
	$activeOddilId = $model->type == 'Oddíl' ? $model->id : 0;
?>

<div class="col-md-3">
	<div class="list-group" id="navigation">
	<?php
		$higherUnits = Unit::find()->where(['type'=>'Středisko'])->orWhere(['type'=>'Okres'])->OrderBy('Code')->all();
		foreach ($higherUnits as $higherUnit)
		{
			if (!$higherUnit->name) continue;

			$name = $higherUnit->name;
			if ($higherUnit->type == "Středisko" && $higherUnit->short_name)
			{
				$name = 'středisko '.$higherUnit->short_name;
			}
			?>

			<div class="list-group-item sidebar_item <?= $activeStrediskoId== $higherUnit->id ? 'active' : '' ?>">
			<?= Html::a($name , [$action, 'id' => $higherUnit->id] + $extra_params)?>
			<?php if ($showSubunits && $higherUnit->type == 'Středisko') { ?>
				<a href="#navigation-<?= $higherUnit->id?>" data-parent="#navigation" data-toggle="collapse">
				<span id="icon-<?= $higherUnit->id?>" class="glyphicon glyphicon-chevron-<?= ($activeStrediskoId== $higherUnit->id ? 'down' : 'right')?>" aria-hidden="true"></span>
				</a>
			<?php } ?>
			</div>
				<?php if ($showSubunits  && $higherUnit->type == 'Středisko') { ?>
					<div id="navigation-<?=$higherUnit->id?>" class="submenu sidebar_item pannel-collapse collapse  <?= ($activeStrediskoId== $higherUnit->id ? 'in' : '')?>" aria-expanded="true">
					<?php
						foreach ($higherUnit->units as $oddil)
						{
							echo Html::a($oddil->name, [$action, 'id' => $oddil->id]  + $extra_params, ['class' => 'list-group-item list-group-item-info '. 
																($activeOddilId== $oddil->id ? 'active' : '')]);
						}
					?>
					</div>
					<?
					$this->registerJs("$('#navigation-$higherUnit->id').on('hide.bs.collapse', function() { $('#icon-$higherUnit->id').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-right') } );");
					$this->registerJs("$('#navigation-$higherUnit->id').on('show.bs.collapse', function() { $('#icon-$higherUnit->id').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down') } );");
				}
		}
	?>
	</div>
</div>

<div class="col-md-9">
<h1><?= $this->title; ?></h1>
<?php
echo Nav::widget([
    'items' => $this->params['topMenu'],
    'options' => ['class' =>'nav-tabs'], // set this to nav-tab to get tab-styled navigation
]);
?>


<?= $content ?>
</div>