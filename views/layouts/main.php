﻿<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

use webvimark\modules\UserManagement\components\GhostNav;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
</head>
<body>
<?php $this->beginBody() ?>


       

<div class="wrap">


        

        <div class="container">

                <?php
            NavBar::begin([
                
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar navbar-default navbar-static-top',
                    'style' => 'float: right'
                ],
            ]);
            echo GhostNav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],

                'items' => [
                    ['label' => 'Domů', 'url' => ['/site/index'], 'visible'=>true],
                    
                    /*
                    
                    ['label' => 'Okresní rada',
                     'items' => [
                    	 ['label' => 'Kontakty, lidé', 'url' => ['/site/orj'], 'visible'=>true],
                    	 ['label' => 'Kalendář', 'url' => ['/site/kalendar'], 'visible'=>true],
                    	 ['label' => 'Dokumenty', 'url' => ['/site/dokumenty'], 'visible'=>true],
                    	 ['label' => 'Akce a fotky', 'url' => ['/site/akce'], 'visible'=>true],
                    	 ],
                    ],
                    ['label' => 'Skauti v okrese',
                     'items' => [
                    	['label' => 'Střediska', 'url' => ['/site/onas'], 'visible'=>true],
	                    ['label' => 'Grafy', 'url' => ['chart/zakladna-pohlavi', 'id'=>84 ], 'visible'=>true],
	                    ['label' => 'Mapy', 'url' => ['map/heat', 'id'=>84 ], 'visible'=>true], 
	                    ['label' => 'KHMS', 'url' => ['/chart/results-overall'], 'visible'=>true],
	                    ],
                    ],
                    
                    */
                    
                    ['label' => 'ORJ', 'url' => ['/site/orj'], 'visible'=>true],
                    ['label' => 'Skauti v okrese', 'url' => ['/site/onas'], 'visible'=>true],
                    ['label' => 'Statistiky', 
                     'items' => [
	                    ['label' => 'Grafy', 'url' => ['chart/zakladna-pohlavi', 'id'=>84 ], 'visible'=>true],
	                    ['label' => 'Mapy', 'url' => ['map/heat', 'id'=>84 ], 'visible'=>true], 
	                    ['label' => 'KHMS', 'url' => ['/chart/results-overall'], 'visible'=>true],
	                    ],
                    ],
                    ['label' => 'Akce a kalendář', 'url' => ['/site/kalendar'], 'visible'=>true],
                    ['label' => 'Dokumenty', 'url' => ['/site/dokumenty'], 'visible'=>true],

                    
                    
                    
                    ['label' => 'Editace', 
                     'items' => [
                     	['label' => 'Novinky', 'url' => ['/news/index']],
	                    ['label' => 'Jednotky', 'url' => ['/unit/index']],
	                    ['label' => 'Lidi', 'url' => ['/person/index']],
	                    ['label' => 'Klubovny', 'url' => ['/klubovna/index']],
	                    ['label' => 'Dokumenty', 'url' => ['/document/index']],
	                    ['label' => 'Funkce', 'url' => ['/funkce/index']],
	                    ['label' => 'Úkoly', 'url' => ['/ukoly/index']],
	                    ['label' => 'Události', 'url' => ['/event/index']],
	                    ['label' => 'Nahrát', 'url' => ['/upload/upload']],
	                    ['label' => 'KHMS', 
                     		'items' => [
                        	['label' => 'Ročníky', 'url'=> ['/season/index']],
                        	['label' => 'Kritéria', 'url'=> ['/kriteria/index']],
                        	['label' => 'Skupiny kriterii', 'url' => ['/kriteria-group']]
                     		],
                    	],
                     ],
                    ],
                    
                    /*
                    ['label' => 'Contact', 'url' => ['/site/contact']],
                    */
                
                    Yii::$app->user->isGuest ?
                        ['label'=>'Login', 'url'=>['/user-management/auth/login']]:
                        ['label' => Yii::$app->user->identity->username,
                         'items' => [
                            ['label'=>'Uživatelé', 'url' => ['/user-management/user/index']],
                            ['label'=>'Role', 'url' => ['/user-management/role/index']],
                            ['label'=>'Permissions','url' => ['/user-management/permission/index']],
                            ['label'=>'Permission groups', 'url' => ['/user-management/auth-item-group/index']],
                            ['label'=>'Visit log', 'url' => ['/user-management/user-visit-log/index']],
                            ['label'=>'Logout' , 'url' => ['/user-management/auth/logout']],
                        ]]
                ],
            ]);
            NavBar::end();
        ?>


    <div style="position: relative; top: -50px">
    	<a href="<?= Url::to(['site/index']) ?>"><img src="images/main_frame3.png" class="hidden-sm hidden-xs">
    	<img src="images/orjspk_logo.png" class="hidden-sm hidden-xs" style="position: absolute; top: 50px; left: -31px; z-index: 5"></a>
    </div>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; GISkaut <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

</div>
               
    
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
