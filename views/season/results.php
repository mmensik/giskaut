<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Season */

$this->title = 'Výsledky: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ročník', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Výsledky';

?>
<div class="season-update">

    <h1><?= Html::encode($this->title) ?></h1>

<?php
    	$form = ActiveForm::begin();
    ?>
    <table class="table table-header-rotated">
    <thead>
    <tr>

    	<th><?= Html::submitButton('Ulozit', ['class' => 'btn btn-primary']);?></th>

    <?php
    	foreach ($model->units as $unit) {
    ?>
    		<th class="rotate"><div><span><?= $unit->name ?></span></div></th>
    <?php
    	}
    ?>

    </tr>
    </thead>
	
    

	<tbody>
	<?php
    	foreach ($model->kriterias as $kriteria) {
    ?>
    	<tr>
    		<th class="row-header"><?= $kriteria->short_description ?></th>
    		<?php foreach ($model->units as $unit) {
    		?>
    		<td><?php echo $form->field($results[$kriteria->id][$unit->id], "[$kriteria->id][$unit->id]points")->label(false);?></td>
    		<?php } ?>
    	</tr>
    	<?php
    		}
    	?>

    	</tbody>
   
    </table>

    <?php
    	$form = ActiveForm::end();
    ?>

</div>
