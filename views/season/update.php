<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Season */

$this->title = 'Úpravy ročníku ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ročník', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Úpravy';
?>
<div class="season-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="season-form">
	    <?php $form = ActiveForm::begin(); ?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Vytvořit' : 'Upravit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>

	    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'isActive')->checkbox() ?>
	<?php
	echo Tabs::widget([
	    'items' => [
	    	[
	        	'label' => 'Kritéria',
	        	'content' => $this->render('_kriteria', ['form'=>$form, 'model'=>$model]),
	        	'active' => true
	        ],
	        [
	            'label' => 'Jednotky',
	            'content' => $this->render('_units', ['form'=>$form, 'model'=>$model]),
	            
	        ],
    	],
    ]);
    ?>
    	    <?php ActiveForm::end(); ?>
	</div>
</div>
