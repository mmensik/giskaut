<?php
	use app\models\Kriteria;
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;

	//Unit::ListStredisko();


	$items = [];

	foreach (Kriteria::find()->all() as $kriteria) {
		$items[$kriteria->id] = $kriteria->short_description;
	}

	echo $form->field($model, 'kriteria_ids')->checkBoxList($items);

	
?>