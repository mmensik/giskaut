﻿<?php

use app\models\Kriteria;

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\bootstrap\ActiveForm;



    ?>
    <div style="border: solid 0px red; clear: both">

		<h2>Seznam všech kritérií</h2>

		<p class="normal">
		<table class="KHMS_table" border=1>
		<tr><!--<td></td>//--><td class="KHMS_table" style="text-align: right; width: 100px"><b>Termín vyhodnocení</b></td><td class="KHMS_table" style="text-align: left;"><b>Kritérium</b></td><td style="text-align: center; width: 50px"><b>Body</b></td><td style="text-align: center; width: 190px"><b>Skupina kritérií</b></td></tr>
		
		<?
        $kriteria = Kriteria::find()->OrderBy('termin')->all();
		foreach ($kriteria as $kriterium) {
			
			$termin_month=$kriterium->termin ? date_create($kriterium->termin)->format('m'):0;
		    if ($termin_month==12||$termin_month==1||$termin_month==2):
	    		$bg_color="#eef6ff";
	    	elseif ($termin_month==3||$termin_month==4||$termin_month==5):
	    		$bg_color="#efffe6";
	    	elseif ($termin_month==6||$termin_month==7||$termin_month==8):
	    		$bg_color="#feffea";
	    	elseif ($termin_month==9||$termin_month==10||$termin_month==11):
	    		$bg_color="#fff0f4";
	    	else:
	    		$bg_color="";			
	    	endif;	
			
			echo "<tr height=20><!--<td style=\"text-align: right;\">".$kriterium->id."</td>//-->
                    <td class=\"KHMS_table\" style=\"text-align: right; background: ".$bg_color."\">". ($kriterium->termin ? date_create($kriterium->termin)->format('j.m.') : "průběžně") ."</td>
                    <td class=\"KHMS_table\" style=\"text-align: left;\">".$kriterium->short_description."</td>
                    <td style=\"text-align: center\">".($kriterium->points_per_unit * $kriterium->basic_unit)."</td>
                    <td style=\"text-align: center\">".$kriterium->kriteriaGroup->name."</td></tr>";
		}

    	?>
    	</table>
    	<br>
    	</div>
    
    
    
    <h2>Kompletní tabulka bodů</h2>
    
    <table class="table table-header-rotated">
    <thead>
    <tr>

    	<th></th>

    <?php
    	foreach ($units as $unit) {
    ?>
    		<th class="rotate"><div><span><?= $unit->short_name ?></span></div></th>
    <?php
    	}
    ?>

    </tr>
    </thead>
	

	<tbody>
	<?php
    	foreach ($kriterias as $kriteria) {
    ?>
    	<tr>
    		<th class="row-header"><?= $kriteria->short_description ?></th>
    		<?php foreach ($units as $unit) {
                $result = $results[$kriteria->id][$unit->id];
    		?>
    		<td><?php echo $result->pointResult ?></td>
    		<?php } ?>
    	</tr>
    	<?php
    		}
    	?>
    	</tbody>
   
    </table>


</div>
